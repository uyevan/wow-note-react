import type {Config} from 'tailwindcss'
import {nextui} from "@nextui-org/react";

const config: Config = {
    content: [
        './pages/**/*.{js,ts,jsx,tsx,mdx}',
        './components/**/*.{js,ts,jsx,tsx,mdx}',
        './app/**/*.{js,ts,jsx,tsx,mdx}',
        './node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}',
        "./node_modules/@nextui-org/theme/dist/components/button.js",
    ],
    theme: {
        extend: {
            backgroundImage: {
                'body-light': "url('/assets/image/demo.png')",
                'body-dark': "url('/assets/image/demo_dark.png')",
            }
        },
    },
    darkMode: "class",
    plugins: [nextui({
        addCommonColors: true,
    })],
}
export default config
