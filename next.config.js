/** @type {import('next').NextConfig} */
const nextConfig = {
    /*代理方式（1）*/
    async rewrites() {
        return {
            fallback: [
                //道理接口请求
                /*
                生产环境(Prod)：http://127.0.0.1:9091 (代理:https://server.jfkj.xyz)
                开发环境(Dev)：http://127.0.0.1:9091 (代理:https://localhost:9091)
                 */
                {source: '/:path*', destination: `https://server.jfkj.xyz/:path*`},

            ]
        }
    },
    /*打包方式*/
    //output: 'standalone',
    /*构建ID*/
    generateBuildId: async () => {
        return (process.env.VERSION || Date.now() + Math.round(Math.random() * 2441139)).toString()
    },
    reactStrictMode: false //关闭Strict模式
}
module.exports = nextConfig
