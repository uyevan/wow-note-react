/*用户验证配置*/
import {decodeFromBase64} from "next/dist/build/webpack/loaders/utils";
import cookie from "react-cookies";
import {Token, UserName} from "../constants/authTokenConstants";
import {validate} from "../utils/validation";

/**
 * 全局认证检测
 * @constructor
 */
export const AuthenticationChecker = () => {
    const usernameT = cookie.load(UserName);
    const token = cookie.load(Token);
    /*已登录*/
    if (!validate(usernameT) && !validate(token)) {
        const username = decodeFromBase64(usernameT);
        /*令牌用户数据验证操作*/
        return [true, username];
    }
    /*清理Cookie*/
    cookie.remove(UserName)
    cookie.remove(Token)
    return [false, ''];
}