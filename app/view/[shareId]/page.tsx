'use client'
import ViewSharePage from "@/app/view/[shareId]/ViewSharePage";
import React, {useEffect, useState} from "react";

export default function View({params}: { params: { shareId: string } }) {
    return (
        <ViewSharePage s_id={params.shareId}/>
    );
}