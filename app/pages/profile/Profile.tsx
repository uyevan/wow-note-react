/*用户中心*/
import React, {useEffect, useMemo, useRef, useState} from "react"
import {
    Avatar,
    BreadcrumbItem,
    Breadcrumbs,
    Button,
    Card,
    CardHeader,
    Chip,
    Code,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownTrigger,
    Image,
    Modal,
    ModalBody,
    ModalContent,
    ModalFooter,
    ModalHeader,
    Spinner,
    Switch,
    Table,
    TableBody,
    TableCell,
    TableColumn,
    TableHeader,
    TableRow,
    useDisclosure
} from "@nextui-org/react"
import FolderService from "@/app/service/folderService"
import NoteService from "@/app/service/noteService"
import {useAsyncList} from "@react-stately/data"
import {AiFillHome, AiOutlineAppstore, AiOutlineBars} from "react-icons/ai"
import {CardBody, CardFooter} from "@nextui-org/card"
import {BiDotsVerticalRounded, BiFolderOpen, BiPlus} from "react-icons/bi"
import {useLocation, useSearchParams} from "react-router-dom"
import NoteEdit from "@/app/components/profile/note/noteEdit"
import ToastUtil from "@/app/utils/toastUtil"
import CreateFolder from "@/app/components/profile/folder/createFolder"
import CreateNote from "@/app/components/profile/note/createNote"
import {delay} from "framer-motion"
import DelNoteService from "@/app/service/components/delNoteService"
import DelFolderService from "@/app/service/components/delFolderService"
import CreateShareService from "@/app/service/components/createShareService"
import ShareModifyService from "@/app/service/shareModifyService"
import {List} from "postcss/lib/list";
import {Emoji} from "@lezer/markdown";
import {DeleteIcon} from "@nextui-org/shared-icons";
import {Cookie} from "next/dist/compiled/@next/font/dist/google";
import {cookies} from "next/headers";
import cookie from "react-cookies";
import {inFifteenMinutes} from "@/app/constants/authTokenConstants";
import {any, number} from "prop-types";
import FindNoteService from "@/app/service/FindNoteService";
import {decodeFromBase64} from "next/dist/build/webpack/loaders/utils";
import ExitLogin from "@/app/components/profile/exitLogin";
import SettingPage from "@/app/components/profile/SettingPage";

/**
 * 个人中心文件夹列表和笔记列表渲染页
 * @param searchKey
 * @param setSearchKey
 * @param inSearch
 * @param setInSearch
 * @constructor
 */
export default function Profile({searchKey, setSearchKey, inSearch, setInSearch}: any) {
    //第一种方式 ---> 会更新当前状态 所以不能返回上一个状态
    // const [path, setPath] = useState(params.path);
    /**第二种方式 ---> 会更新URL参数 可以返回上一个状态*/
    const [search, setSearch] = useSearchParams()
    /**存储路由*/
    const [path, setPath] = useState(["/"])
    /**是否已加载状态定义*/
    const location = useLocation()
    const [isLoadingFolder, setIsLoadingFolder] = useState(true)
    const [isLoadingNote, setIsLoadingNote] = useState(true)
    const {isOpen, onOpen, onClose, onOpenChange} = useDisclosure()
    /**创建文件夹Modal状态*/
    const {isOpen: isOpenFolder, onOpen: onOpenFolder, onOpenChange: onOpenChangeFolder} = useDisclosure()
    /**创建笔记Modal状态*/
    const {isOpen: isOpenNote, onOpen: onOpenNote, onOpenChange: onOpenChangeNote} = useDisclosure()
    /**确认删除Modal状态*/
    const {isOpen: isOpenDelNote, onOpen: onOpenDelNote, onOpenChange: onOpenChangeDelNote} = useDisclosure()
    /**v确认删除Modal状态*/
    const {isOpen: isOpenDelFolder, onOpen: onOpenDelFolder, onOpenChange: onOpenChangeDelFolder} = useDisclosure()
    /**创建分享状态*/
    const {isOpen: isOpenShare, onOpen: onOpenShare, onOpenChange: onOpenChangeShare} = useDisclosure()
    /**笔记Id临存*/
    const [noteId, setNoteId] = useState("")
    /**删除的笔记id和title*/
    const [delData, setDelData] = useState("")
    /**笔记排列方式*/
    const [noteIfGrid, setNoteIfGrid] = useState(cookie.load('noteIfGrid') == undefined ? 'false' : cookie.load('noteIfGrid'))
    /**目录排列方式*/
    const [folderIfGrid, setFolderIfGrid] = useState(cookie.load('folderIfGrid') == undefined ? 'false' : cookie.load('folderIfGrid'))
    /** 防止第一次重复请求 */
    const [enable, setEnable] = useState(false);

    /**获取文件夹数据*/
    let listFolder = useAsyncList({
        async load({signal}) {
            const path = search.get("path")
            const folderData = await FolderService(path === null ? "root" : path)
            setEnable(true)//结束Enable（防止重复请求）
            if (folderData !== undefined && folderData !== null && folderData.status) {
                return {
                    items: folderData.data
                }
            } else {
                return {
                    items: []
                }
            }
        }
    })

    /**获取笔记数据*/
    let listNote = useAsyncList({
        async load({signal}) {
            setInSearch(false);
            const path = search.get("path")
            const noteData = await NoteService(path === null ? "root" : path)
            setEnable(true)//结束Enable（防止重复请求）
            if (noteData !== undefined && noteData !== null && noteData.status) {
                return {
                    items: noteData.data
                }
            } else {
                return {
                    items: []
                }
            }
        }
    })
    /**搜索笔记*/
    let findNote = useAsyncList({
        async load({signal}) {
            if (searchKey != '' && searchKey != undefined) {
                const findData = await FindNoteService(searchKey);
                if (findData !== undefined && findData !== null && findData.status) {
                    setInSearch(true)
                    ToastUtil(findData?.message, '🤪', 'success')
                    return {items: findData.data}
                } else {
                    ToastUtil(findData?.message, '🤨', 'warning')
                    return {
                        items: []
                    }
                }
            }
            return {
                items: []
            }
        }
    })

    /**订阅消息，接受消息时进行列表渲染*/
    PubSub.unsubscribe('keyword');
    PubSub.subscribe('keyword', (_, data) => {
        if (data == '' || data == undefined) {
            setSearchKey('');
            listNote.reload();
        } else {
            setSearchKey(data);
            findNote.reload();
        }
    });
    /**创建一个消息监听器，列表发生变化重新渲染.*/
    //文件夹
    PubSub.unsubscribe("refreshFolder")
    PubSub.subscribe("refreshFolder", (_, data) => {
        listFolder.reload()//重新加载
    })
    //笔记
    PubSub.unsubscribe("refreshNote")
    PubSub.subscribe("refreshNote", (_, data) => {
        listNote.reload()
    })


    /**检测location地址更新状态的Hook钩子（就是自动执行的监听器）*/
    useEffect(() => {
        setIsLoadingNote(true)
        setIsLoadingFolder(true)

        async function fetchData() {
            /*更新路由栏路径*/
            const localUrl = search.get("path")
            if (localUrl === null || localUrl === "root" || localUrl === "/") {
                setPath(["/"])
            } else {
                const newPathList = [...path] //复制当前路径列表
                //查找新路径在当前路径列表中的位置
                const index = newPathList.indexOf(localUrl)
                if (index !== -1) {
                    // 如果存在，则删除该路径后面的所有值
                    newPathList.splice(index + 1, newPathList.length - index - 1)
                } else {
                    //如果不存在，则将新路径合并到当前路径列表中
                    newPathList.push(...getPathParts(localUrl))
                }
                /*更新路由*/
                setPath(newPathList)
            }
            //在path或location变化时重新加载数据  路径发生了变化
            if (enable) {
                listFolder.reload()
                listNote.reload()
            }
            //更新加载中状态
            delay(() => {
                setIsLoadingFolder(false)
                setIsLoadingNote(false)
            }, 100)
        }

        fetchData().then(r => {
            /*执行fetchData时出错才执行*/
        })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    /**存在目录时渲染函数*/
    function List(listFolder: any, listNote: any) {
        return (
            <>
                <div
                    className={"flex-col justify-center flex-grow items-start bg-white dark:bg-default-100 p-3 space-y-3"}>
                    <Breadcrumbs key={"boy"} variant={"light"} color={"foreground"} size={"md"}
                                 hideSeparator /*隐藏层级icon*/
                                 classNames={{
                                     list: "gap-3"
                                 }}
                                 itemClasses={{
                                     item: [
                                         "px-2 py-0.5 border-small border-default-400 rounded-small",
                                         "data-[current=true]:border-default-800 data-[current=true]:bg-foreground data-[current=true]:text-background transition-colors",
                                         "data-[disabled=true]:border-default-400 data-[disabled=true]:bg-default-100",
                                         "dark:text-white light:text-black dark:bg-black"
                                     ]
                                 }}
                    >
                        {
                            path.map((segment, index) => (
                                <BreadcrumbItem onPress={() => {
                                    /*当前目录则无效果   相同不会更新 所以不用判断*/
                                    /*if (segment === search.get('path')) {
                                        return;
                                    }*/
                                    if (segment === "/") {
                                        setPath(["/"])
                                        setSearch("")
                                        return
                                    }
                                    setSearch(`path=${segment}`)
                                }} key={segment} startContent={segment === "/" ?
                                    <AiFillHome/> : <BiFolderOpen/>}>{segment}</BreadcrumbItem>
                            ))
                        }
                    </Breadcrumbs>
                    {/*目录渲染*/}
                    {
                        //加载中状态判断
                        isLoadingFolder ?
                            <Card className={"h-1/3"} shadow={"md"} radius={"lg"}>
                                <CardBody>
                                    <div className={"flex justify-center flex-grow items-center pb-4"}>
                                        <Spinner label="加载ing...😵‍💫" color="primary" labelColor="primary"
                                                 size={"sm"}/>
                                    </div>
                                </CardBody>
                            </Card> : (
                                folderIfGrid == 'true' && cookie.load('folderIfGrid') != undefined ?
                                    <Card className="w-full">
                                        <CardHeader className="justify-between">
                                            {createFolder()}
                                        </CardHeader>
                                        <CardBody>
                                            {
                                                listFolder.items.length === 0 ?
                                                    <div className={"flex justify-center flex-grow items-center pb-4"}>
                                                        <Code color="warning"> 暂无文件夹哦 🧐 </Code>
                                                    </div> :
                                                    <div className="gap-2 grid grid-cols-2 sm:grid-cols-6">
                                                        {listFolder.items.map((item: any, index: bigint) => (
                                                            <div key={index}
                                                                 className="relative border-0 rounded-xl overflow-hidden shadow-0"
                                                                 onClick={(key) => {
                                                                     /*setPath(key) 我使用第二种方式*/
                                                                     /*设置新的目录ID*/
                                                                     setSearch(`path=${item.id}`)
                                                                     /*const newPath = [...path, `${key}`];
                                                                     setPath(newPath);*/
                                                                     //更新加载中状态
                                                                     setIsLoadingNote(true)
                                                                     setIsLoadingFolder(true)
                                                                 }}
                                                            >
                                                                <div className="ps-3 pe-3 pt-3 pb-3 text-center">
                                                                    <p className={`text-6xl mb-5`}>{item.icon}</p>
                                                                    <h3 className="text-[13px] text-gray-600 mb-1 dark:text-white light:text-black line-clamp-1">{item.title}</h3>
                                                                    <h3 className="text-xs text-gray-500 mb-2 line-clamp-1">{item.content}</h3>
                                                                    <p className="text-xs text-gray-500 mb-1">{item.createTime}</p>
                                                                    <div className={`flex justify-between`}>
                                                                        <Switch isSelected={false}
                                                                                color="default" size={"sm"}
                                                                                onValueChange={() => {
                                                                                    ToastUtil("不支持分享目录", "❎", "error")
                                                                                }}/>
                                                                        <div
                                                                            className="relative flex justify-center items-end gap-2">
                                                                            <Dropdown backdrop={"transparent"}
                                                                                      className="bg-background border-1 border-default-200">
                                                                                <DropdownTrigger>
                                                                                    <Button isIconOnly radius="full"
                                                                                            size="sm"
                                                                                            variant="light">
                                                                                        <BiDotsVerticalRounded
                                                                                            className="text-default-400"/>
                                                                                    </Button>
                                                                                </DropdownTrigger>
                                                                                <DropdownMenu onAction={(key) => {
                                                                                    setDelData(key.toString())
                                                                                    onOpenDelFolder()
                                                                                }}>
                                                                                    <DropdownItem color={"danger"}
                                                                                                  className={`dark:text-white light:text-black `}
                                                                                                  key={item.id}
                                                                                                  startContent={
                                                                                                      <DeleteIcon/>}>删
                                                                                        除</DropdownItem>
                                                                                </DropdownMenu>
                                                                            </Dropdown>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        ))}
                                                    </div>
                                            }
                                        </CardBody>
                                    </Card>
                                    :
                                    listFolder.items.length === 0 ?
                                        DontExitsFolder("暂无文件夹哦 🧐") :
                                        <Table
                                            title={"我的文件夹"}
                                            aria-label="我的所有文件夹."
                                            shadow={"md"}/*阴影*/
                                            hideHeader={false}/*隐藏头部*/
                                            isStriped={false}/*条纹化*/
                                            isHeaderSticky={true}/*头部阴影*/
                                            topContent={
                                                <div className={"flex justify-between items-center"}>
                                                    {createFolder()}
                                                </div>
                                            }
                                            onClick={() => {
                                            }}
                                            onRowAction={(key) => {
                                                /*setPath(key) 我使用第二种方式*/
                                                /*设置新的目录ID*/
                                                setSearch(`path=${key}`)
                                                /*const newPath = [...path, `${key}`];
                                                setPath(newPath);*/
                                                //更新加载中状态
                                                setIsLoadingNote(true)
                                                setIsLoadingFolder(true)
                                            }
                                            }/*获取key 这里是ID*/
                                            fullWidth={true}
                                        >
                                            <TableHeader>
                                                <TableColumn align={"center"} key="num">
                                                    #
                                                </TableColumn>
                                                <TableColumn align={"center"} key="title">
                                                    目录名
                                                </TableColumn>
                                                <TableColumn className={`w-full`} align={"center"} key="content">
                                                    目录简介
                                                </TableColumn>
                                                <TableColumn align={"center"} key="createTime">
                                                    创建时间
                                                </TableColumn>
                                                {/*<TableColumn align={"center"} key="id">*/}
                                                {/*    目录号*/}
                                                {/*</TableColumn>*/}
                                                <TableColumn align={"center"} key="status">
                                                    分享状态
                                                </TableColumn>
                                                <TableColumn align={"center"} key="actions">
                                                    操作
                                                </TableColumn>
                                            </TableHeader>
                                            {/*设置内容*/}
                                            <TableBody
                                                items={listFolder.items}
                                                isLoading={isLoadingFolder}
                                                loadingContent={<Spinner label="加载中..."/>}
                                            >
                                                {(item: any) => (
                                                    <TableRow key={item.id} className={"h-10"}>
                                                        {(columnKey) =>
                                                            <TableCell>{renderCell(item, columnKey, true)}</TableCell>
                                                        }
                                                    </TableRow>
                                                )}
                                            </TableBody>
                                        </Table>
                            )
                    }

                    {/*笔记渲染*/}
                    {
                        //加载中状态判断
                        isLoadingNote ?
                            <Card className={"h-1/3"} shadow={"md"} radius={"lg"}>
                                <CardBody>
                                    <div className={"flex justify-center flex-grow items-center pb-4"}>
                                        <Spinner label="加载ing...😵‍💫" color="primary" labelColor="primary"
                                                 size={"sm"}/>
                                    </div>
                                </CardBody>
                            </Card> : (
                                noteIfGrid == 'true' && cookie.load('noteIfGrid') != undefined ?
                                    /*新自定义列表*/
                                    <Card className="w-full">
                                        <CardHeader className="justify-between">
                                            {createNote()}
                                        </CardHeader>
                                        <CardBody>
                                            {
                                                (inSearch ? findNote.items : listNote.items).length === 0 ?
                                                    <div className={"flex justify-center flex-grow items-center pb-4"}>
                                                        <Code
                                                            color="warning"> {inSearch ? "没搜到笔记哦 🥸" : "暂无笔记哦 🥸"} </Code>
                                                    </div> :
                                                    <div className="gap-2 grid grid-cols-2 sm:grid-cols-6">
                                                        {(inSearch ? findNote.items : listNote.items).map((item: any, index: number) => (
                                                            <div key={item.id}
                                                                 className="relative border-0 rounded-xl overflow-hidden shadow-0"
                                                                 onClick={(key) => {
                                                                     /*打开阅读界面*/
                                                                     setNoteId(item.id)
                                                                     onOpen()
                                                                 }}
                                                            >
                                                                <div className="ps-3 pe-3 pt-3 pb-3 text-center">
                                                                    <p className={`text-6xl mb-5`}>{item.icon}</p>
                                                                    <h3 className="text-[13px] text-gray-600 mb-2 line-clamp-1 dark:text-white light:text-black ">{item.title}</h3>
                                                                    <p className="text-xs text-gray-500 mb-1">{item.updateTime}</p>
                                                                    <div className={`flex justify-between`}>
                                                                        <Switch isSelected={item.type != 0}
                                                                                color="secondary"
                                                                                size={"sm"}
                                                                                onValueChange={(isSelected) => {
                                                                                    setNoteId(item.id)
                                                                                    if (!isSelected) {
                                                                                        //选中
                                                                                        ShareModifyService(item.shareId, 0, 0)
                                                                                            .then(r => {

                                                                                            })

                                                                                    } else {
                                                                                        //未选中
                                                                                        //判断是否已经在分享文档里
                                                                                        if (item.shareId === "0") {
                                                                                            //未分享过
                                                                                            onOpenShare()
                                                                                        } else {
                                                                                            //分享过
                                                                                            ShareModifyService(item.shareId, 1, 0)
                                                                                                .then(r => {

                                                                                                })
                                                                                        }
                                                                                    }
                                                                                }}/>
                                                                        <div
                                                                            className="relative flex justify-center items-end gap-2">
                                                                            <Dropdown backdrop={"transparent"}
                                                                                      className="bg-background border-1 border-default-200">
                                                                                <DropdownTrigger>
                                                                                    <Button isIconOnly radius="full"
                                                                                            size="sm"
                                                                                            variant="light">
                                                                                        <BiDotsVerticalRounded
                                                                                            className="text-default-400"/>
                                                                                    </Button>
                                                                                </DropdownTrigger>
                                                                                <DropdownMenu onAction={(key) => {
                                                                                    setDelData(key.toString())
                                                                                    onOpenDelNote()
                                                                                }}
                                                                                >
                                                                                    <DropdownItem color={"danger"}
                                                                                                  key={item.id}
                                                                                                  className={`dark:text-white light:text-black `}
                                                                                                  startContent={
                                                                                                      <DeleteIcon/>}>删
                                                                                        除</DropdownItem>
                                                                                </DropdownMenu>
                                                                            </Dropdown>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        ))}
                                                    </div>
                                            }
                                        </CardBody>
                                    </Card>
                                    :
                                    (inSearch ? findNote.items : listNote.items).length === 0 ?
                                        DontExitsNote(inSearch ? "没搜到笔记哦 🥸" : "暂无笔记哦 🥸") :
                                        <Table
                                            title={"我的云笔记"}
                                            aria-label="我的所有云笔记." color={"primary"}
                                            shadow={"md"}/*阴影*/
                                            hideHeader={false}/*隐藏头部*/
                                            isStriped={false}/*条纹化*/
                                            isHeaderSticky={true}/*头部阴影*/
                                            topContent={
                                                <div className={"flex justify-between items-center"}>
                                                    {createNote()}
                                                </div>
                                            }
                                            onRowAction={(key) => {   /*打开阅读界面*/
                                                setNoteId(key.toString())
                                                onOpen()
                                            }
                                            }/*获取key 这里是ID*/
                                            fullWidth={true}
                                        >
                                            <TableHeader>
                                                <TableColumn align={"center"} key="num">
                                                    #
                                                </TableColumn>
                                                <TableColumn className={`w-full`} align={"center"} key="title">
                                                    笔记名
                                                </TableColumn>
                                                <TableColumn align={"center"} key="updateTime">
                                                    修改时间
                                                </TableColumn>
                                                {/*<TableColumn align={"center"} key="createTime">
                                                创建时间
                                            </TableColumn>
                                            <TableColumn align={"center"} key="id">
                                                笔记号
                                            </TableColumn>*/}
                                                <TableColumn align={"center"} key="status">
                                                    分享状态
                                                </TableColumn>
                                                <TableColumn align={"center"} key="actions">
                                                    操作
                                                </TableColumn>
                                            </TableHeader>
                                            {/*设置内容*/
                                            }
                                            <TableBody
                                                items={inSearch ? findNote.items : listNote.items}
                                                isLoading={isLoadingNote}
                                                loadingContent={<Spinner label="加载中..."/>}
                                            >
                                                {/*渲染list*/}
                                                {(item: any) => (
                                                    <TableRow key={item.id} className={"h-10"}>
                                                        {(columnKey) =>
                                                            <TableCell>{renderCell(item, columnKey, false)}</TableCell>
                                                        }
                                                    </TableRow>
                                                )}
                                            </TableBody>
                                        </Table>
                            )

                    }

                    {/*编辑器*/}
                    <NoteEdit isOpen={isOpen} onOpenChange={onOpenChange} noteId={noteId}
                              folderId={search.get("path")}/>
                    {/*创建文件夹*/}
                    <CreateFolder isOpenFolder={isOpenFolder} onOpenChangeFolder={onOpenChangeFolder}
                                  parentId={search.get("path")}/>
                    {/*创建笔记*/}
                    <CreateNote isOpenNote={isOpenNote} onOpenChangeNote={onOpenChangeNote}
                                folderId={search.get("path")}/>
                    {/*删除笔记*/}
                    <DelNoteService isOpenDelNote={isOpenDelNote} onOpenChangeDelNote={onOpenChangeDelNote}
                                    noteId={delData}/>
                    {/*删除文件夹*/}
                    <DelFolderService isOpenDelFolder={isOpenDelFolder} onOpenChangeDelFolder={onOpenChangeDelFolder}
                                      folderId={delData}/>
                    {/*创建分享*/}
                    <CreateShareService isOpenShare={isOpenShare} onOpenChangeShare={onOpenChangeShare}
                                        noteId={noteId}/>
                </div>
            </>
        )
    }

    /**新建笔记TabBar*/
    function createNote() {
        return (
            <>
                <Chip color="default" variant="light">我的笔记 📚</Chip>
                <Chip className={`ml-auto ps-4 me-4`} color="default" variant="faded" title={"排列方式"}
                      startContent={noteIfGrid == 'true' ? <AiOutlineBars size={18}/> :
                          <AiOutlineAppstore size={18}/>}
                      onClick={(v) => {
                          if (noteIfGrid == 'true' && cookie.load('noteIfGrid') != undefined) {
                              cookie.remove('noteIfGrid')
                              setNoteIfGrid('false')
                          } else {
                              cookie.save('noteIfGrid', 'true', {expires: inFifteenMinutes, path: '/'})
                              setNoteIfGrid('true')
                          }
                      }}></Chip>
                <Button className={"w-min"} color="secondary" endContent={<BiPlus/>}
                        onPress={
                            onOpenNote
                        }>
                    新建笔记
                </Button>
            </>
        )
    }

    /**新建目录TabBar*/
    function createFolder() {
        return (
            <>
                <Chip color="default" variant="light">我的目录 📂</Chip>
                <Chip className={`ml-auto ps-4 me-4`} color="default" variant="faded" title={"排列方式"}
                      startContent={folderIfGrid == 'true' ? <AiOutlineBars size={18}/> :
                          <AiOutlineAppstore size={18}/>}
                      onClick={(v) => {
                          if (folderIfGrid == 'true' && cookie.load('folderIfGrid') != undefined) {
                              cookie.remove('folderIfGrid')
                              setFolderIfGrid('false')
                          } else {
                              cookie.save('folderIfGrid', 'true', {expires: inFifteenMinutes, path: '/'})
                              setFolderIfGrid('true')
                          }
                      }}></Chip>
                <Button className={"w-min"} color="primary" endContent={<BiPlus/>}
                        onPress={
                            onOpenFolder
                        }>
                    新建目录
                </Button>
            </>
        )
    }

    /**不存在的目录时返回的组件函数*/
    function DontExitsFolder(message: string) {
        return (
            <div>
                <Card className={"h-1/3"} shadow={"md"} radius={"lg"}>
                    <CardBody>
                        <div className={"flex justify-between items-center"}>
                            {createFolder()}
                        </div>
                        <div className={"flex justify-center flex-grow items-center pb-4 pt-4"}>
                            <Code color="warning"> {message} </Code>
                        </div>
                    </CardBody>
                </Card>
            </div>
        )
    }

    /**不存在的文件时返回的组件函数*/
    function DontExitsNote(message: string) {
        return (
            <div>
                <Card className={"h-1/3"} shadow={"md"} radius={"lg"}>
                    <CardBody>
                        <div className={"flex justify-between items-center"}>
                            {createNote()}
                        </div>
                        <div className={"flex justify-center flex-grow items-center pb-4 pt-4"}>
                            <Code color="warning"> {message} </Code>
                        </div>
                    </CardBody>
                </Card>
            </div>
        )
    }

    /**获取部分路径*/
    function getPathParts(path: string) {
        return path.split("/").filter(part => part) //将路径分割为部分并过滤掉空白部分
    }

    /**规则化输出数据*/
    const renderCell = React.useCallback((List: any, columnKey: any, isFolder: boolean) => {
        const cellValue = List[columnKey]
        if (isFolder) {
            switch (columnKey) {
                case "num":
                    return (
                        /*<Avatar isBordered color={"primary"} radius={"lg"} name={List.title.substring(0, 1)}/>*/
                        <p className={`text-2xl text-center`}>{List.icon}</p>
                    )
                case "id":
                    return (
                        <Chip className={"capitalize"} color={"primary"} variant={"flat"}
                              size={"sm"}>&nbsp;{List.id}&nbsp;</Chip>
                    )
                case "createTime":
                    return (
                        <Chip color="primary" variant="dot" size={"sm"}>{List.createTime}</Chip>
                    )
                case "status":
                    return (
                        <Switch isSelected={false} color="default" size={"sm"} onValueChange={() => {
                            ToastUtil("不支持分享目录", "❎", "error")
                        }}/>
                    )
                case "title":
                    return (
                        <h6 className={"w-[150px] line-clamp-1 overflow-hidden dark:text-white light:text-black "}>{List.title}</h6>
                    )
                case "content":
                    return (
                        <h6 className={"w-[250px] line-clamp-1 overflow-hidden dark:text-gray-400"}>{List.content}</h6>
                    )
                case "actions":
                    return (
                        <div className="relative flex justify-center items-end gap-2">
                            <Dropdown backdrop={"transparent"} className="bg-background border-1 border-default-200">
                                <DropdownTrigger>
                                    <Button isIconOnly radius="full" size="sm" variant="light">
                                        <BiDotsVerticalRounded className="text-default-400"/>
                                    </Button>
                                </DropdownTrigger>
                                <DropdownMenu onAction={(key) => {
                                    setDelData(key.toString())
                                    onOpenDelFolder()
                                }}>
                                    <DropdownItem color={"danger"}
                                                  className={`dark:text-white light:text-black `}
                                                  key={List.id} startContent={<DeleteIcon/>}>删
                                        除</DropdownItem>
                                </DropdownMenu>
                            </Dropdown>
                        </div>
                    )
                default:
                    return cellValue
            }
        } else {
            switch (columnKey) {
                case "num":
                    return (
                        /*<Avatar isBordered color={"secondary"} radius={"lg"} name={List.title.substring(0, 1)}/>*/
                        <p className={`text-2xl text-center`}>{List.icon}</p>
                    )
                case "id":
                    return (
                        <Chip className={"capitalize"} variant={"flat"} size={"sm"}
                              color={"secondary"}>&nbsp;{List.id}&nbsp;</Chip>
                    )
                case "createTime":
                    return (
                        <Chip color="secondary" variant="dot" size={"sm"}>{List.createTime}</Chip>
                    )
                case "updateTime":
                    return (
                        <Chip color="secondary" variant="dot" size={"sm"}>{List.updateTime}</Chip>
                    )
                case "status":
                    return (
                        <Switch isSelected={List.type != 0} color="secondary" size={"sm"}
                                onValueChange={(isSelected) => {
                                    setNoteId(List.id)
                                    if (!isSelected) {
                                        //选中
                                        /*ToastUtil("请在分享中心关闭分享！", '🦉', "error");*/
                                        ShareModifyService(List.shareId, 0, 0).then(r => {/*处理返回结果*/
                                        })
                                    } else {
                                        //未选中
                                        //判断是否已经在分享文档里
                                        if (List.shareId === "0") {
                                            //未分享过
                                            onOpenShare()
                                        } else {
                                            //分享过
                                            ShareModifyService(List.shareId, 1, 0).then(r => {/*处理返回结果*/
                                            })
                                        }
                                    }
                                }}/>
                    )
                case "title":
                    return (
                        <h6 className={"w-[250px] line-clamp-1 overflow-hidden dark:text-white light:text-black "}>{List.title}</h6>
                    )
                case "actions":
                    return (
                        <div className="relative flex justify-center items-end gap-2">
                            <Dropdown backdrop={"transparent"} className="bg-background border-1 border-default-200">
                                <DropdownTrigger>
                                    <Button isIconOnly radius="full" size="sm" variant="light">
                                        <BiDotsVerticalRounded className="text-default-400"/>
                                    </Button>
                                </DropdownTrigger>
                                <DropdownMenu onAction={(key) => {
                                    setDelData(key.toString())
                                    onOpenDelNote()
                                }}
                                >
                                    <DropdownItem color={"danger"}
                                                  className={`dark:text-white light:text-black `}
                                                  key={List.id} startContent={<DeleteIcon/>}>删
                                        除</DropdownItem>
                                </DropdownMenu>
                            </Dropdown>
                        </div>
                    )
                default:
                    return cellValue
            }
        }
    }, [onOpenDelFolder, onOpenDelNote, onOpenShare])

    //判断是否加载完再返回

    /**等挂在之后再渲染 */
    const [mounted, setMounted] = useState(false)
    useEffect(() => {
        //组件挂载时执行一次Set的操作
        setMounted(true)
    }, [])

    if (!mounted) return null
    /**本Page（页）返回的DOM元素*/
    return List(listFolder, listNote)
}