/*登录组件*/
import React, {useEffect, useState} from "react";
import {
    Button,
    Checkbox,
    CircularProgress,
    Input,
    Spacer
} from "@nextui-org/react";
import axios from "axios";
import ToastUtil from "@/app/utils/toastUtil";
import cookie from 'react-cookies'
import {useRouter} from "next/navigation";
import {encodeToBase64} from "next/dist/build/webpack/loaders/utils";
import {validateParameter} from "@/app/utils/validation";
import LoginService from "@/app/service/loginService";

/**
 * 登录页
 * @constructor
 */
export default function Login() {
    /*路由*/
    const router = useRouter();
    /*Loading状态*/
    const [loading, setLoading] = useState(false);
    /*定义显示隐藏按钮状态*/
    const [isVisible, setIsVisible] = React.useState(false);
    /*更新状态*/
    const toggleVisibility = () => setIsVisible(!isVisible);
    /*账号密码数据和状态*/
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    /*获取消息（username | password）*/
    PubSub.unsubscribe('username')/*解触*/
    PubSub.subscribe('username', (_, data) => {
        setUsername(data)
        /*/接受完取消订阅*/
        PubSub.unsubscribe('username')
    })
    PubSub.unsubscribe('password')/*解触*/
    PubSub.subscribe('password', (_, data) => {
        setPassword(data)
        /*/接受完取消订阅*/
        PubSub.unsubscribe('password')
    })


    /*UI组件*/
    return (
        <>
            <Input
                isClearable
                className={`dark:text-white`}
                id={'username'}
                name={'username'}
                type={"text"}
                label={"用户名"}
                variant={"faded"}
                placeholder={"请输出您的用户名"}
                onClear={() => console.log("cleared")}
                isRequired={true}
                value={username}
                onChange={({target: {value}}) => setUsername(value)}
            />
            <Spacer y={1.5}/>
            <Input
                name={'password'}
                className={`dark:text-white`}
                id={'password'}
                isClearable={false}
                label={"密码"}
                variant={"faded"}
                placeholder={"请输出您的密码"}
                value={password}
                /*endContent={
                    <button className={"focus:outline-none"} type={"button"} onClick={toggleVisibility}>
                        {
                            isVisible ?
                                (<EyeSlashFilledIcon className="text-2xl text-default-400 pointer-events-none"/>)
                                :
                                (<EyeFilledIcon className="text-2xl text-default-400 pointer-events-none"/>)
                        }
                    </button>
                }*/
                type={"password"}
                isRequired={true}
                onChange={({target: {value}}) => setPassword(value)}
            />
            <Spacer y={5}/>
            <Button className={"font-serif"} fullWidth={true} color={"primary"} variant={"shadow"} onClick={() => {
                LoginService({username, password, setLoading}).then(r => {
                })
            }}>确 认 登 录</Button>
            <Spacer y={3}/>
            <div className={"flex justify-center"}>
                <CircularProgress className={!loading ? "hidden" : ""} size={"lg"} color="default"
                                  aria-label="加载中..."/>
            </div>
        </>
    )
}