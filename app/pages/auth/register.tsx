/*注册组件*/
import {Button, checkbox, Checkbox, Chip, CircularProgress, Input, Spacer, Tab, Tabs, user} from "@nextui-org/react";
import React, {useState} from "react";
import {validateParameterRes} from "@/app/utils/validation";
import axios from "axios";
import ToastUtil from "@/app/utils/toastUtil";
import {NotificationIcon} from "@/app/assets/icons/NotificationIcon";
import {useRouter} from "next/navigation";
import PubSub from 'pubsub-js'
import {delay} from "framer-motion";
import UserRegister from "@/app/service/registerService";

/**
 * 注册页
 * @constructor
 */
export default function Register() {
    const router = useRouter();
    /*Loading状态*/
    const [loading, setLoading] = useState(false);

    /*结果状态定义*/
    const [success, setSuccess] = useState(false);
    const [error, setError] = useState(false);
    const [resData, setResData] = useState('');

    /*定义注册数据和状态*/
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const [nickname, setNickname] = useState('');

    /*服务协议Checkbox*/
    const [isChecked, setChecked] = useState(false);

    return (
        <>
            <Input isClearable
                   className={`dark:text-white`}
                   type={"text"}
                   label={"用户名"}
                   variant={"faded"}
                   placeholder={"请输入您的用户名"}
                   onClear={() => console.log("cleared")}
                   onChange={({target: {value}}) => setUsername(value)}
                   isRequired={true}/>
            <Spacer y={1.5}/>
            <Input
                type={"password"}
                className={`dark:text-white`}
                label={"密码"}
                variant={"faded"}
                placeholder={"请输入您的密码"}
                onChange={({target: {value}}) => setPassword(value)}
                isRequired={true}/>
            <Spacer y={1.5}/>
            <Input isClearable
                   className={`dark:text-white`}
                   type={"text"}
                   label={"名称"}
                   variant={"faded"}
                   placeholder={"请输入您的名称"}
                   onClear={() => console.log("cleared")}
                   onChange={({target: {value}}) => setNickname(value)}
                   isRequired={true}/>
            <Spacer y={1.5}/>
            <Input
                type={"email"}
                className={`dark:text-white`}
                label={"邮箱"}
                variant={"faded"}
                placeholder={"请输入您的邮箱"}
                onClear={() => console.log("cleared")}
                onChange={({target: {value}}) => setEmail(value)}
                isRequired={true}/>
            <Spacer y={1.5}/>
            <div className={"flex py-2 px-1 justify-between"}>
                <Checkbox classNames={{label: "text-small"}} onValueChange={() => {
                    setChecked(!isChecked)
                }}>
                    同意服务协议和隐私协议
                </Checkbox>
            </div>
            <Spacer y={5}/>
            <Button className={"font-serif"} fullWidth={true} color={"primary"} variant={"shadow"} onClick={() => {
                /*调用注册服务*/
                UserRegister({
                    setLoading,
                    setSuccess,
                    setError,
                    setResData,
                    isChecked,
                    username,
                    password,
                    email,
                    nickname
                }).then(r => {
                })
            }}>确 认 注 册</Button>
            <Spacer y={3}/>
            {/*Loading*/}
            <div className={'flex justify-center'}>
                <CircularProgress className={!loading ? 'hidden' : ''} color={"default"} size={"lg"}
                                  aria-label={'加载中...'}/>
            </div>
            <Spacer y={0.5}/>
            <div className={'flex justify-center'}>
                {/*灵动岛😎*/}
                <Chip
                    className={!success && !error ? 'hidden' : ''}
                    endContent={<NotificationIcon size={18}/>}
                    variant="flat"
                    color={success ? error ? 'danger' : 'success' : 'danger'}
                >
                    {resData}
                </Chip>
            </div>
        </>
    )
}