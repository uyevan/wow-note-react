import toast from "react-hot-toast";
import {BorderRadius, Duration} from "@/app/constants/ToastConstants";
import PubSub from "pubsub-js"

/*Toast工具*/
/*Alert 用的 react-hot-toast 库*/
/**
 * 弹窗工具类
 * @param msg
 * @param emoji
 * @param type
 * @constructor
 */
export default function ToastUtil(msg: string, emoji: string, type: 'success' | 'error' | 'warning') {
    /*成功状态*/
    if (type === 'success') {
        toast.success(msg, {
            icon: emoji,
            style: {
                borderRadius: `${BorderRadius}px`,
                background: '#17c964',
                color: 'white',
                fontFamily: 'revert-layer'
            },
            duration: Duration
        })
        return;
    }
    /*失败状态*/
    if (type === 'error') {
        toast.error(msg, {
            icon: emoji,
            style: {
                borderRadius: `${BorderRadius}px`,
                background: '#f31260',
                color: 'white',
                fontFamily: 'revert-layer'
            },
            duration: Duration
        });
        return;
    }
    /*警告状态*/
    if (type === 'warning') {
        toast.error(msg, {
            icon: emoji,
            style: {
                borderRadius: `${BorderRadius}px`,
                background: '#F5A524',
                color: 'white',
                fontFamily: 'revert-layer'
            },
            duration: Duration
        });
        return;
    }
    /*默认状态*/
    toast(msg, {
        icon: emoji,
        style: {borderRadius: `${BorderRadius}px`, background: '#9353d3', color: 'white', fontFamily: 'revert-layer'},
        duration: Duration
    })
}