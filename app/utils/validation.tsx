/*空判断工具*/
import toast from "react-hot-toast";
import {BorderRadius, Duration} from "@/app/constants/ToastConstants";

/**
 * 变量空判断
 * @param value
 */
function validate(value: string): boolean {
    return value === undefined || value === null || value === '';
}

/**
 * params参数验证
 * @param params {...params}
 * @return boolean
 */
function validateParameter(params: Record<string, string | null | undefined> /*接受键值对*/): boolean {
    /*循环判断*/
    for (const paramName in params) {
        const param = params[paramName];
        if (param === undefined || param === null || param === '') {
            /*read参数名*/
            toast.error(`参数 ${paramName} 不能留空`, {
                style: {
                    borderRadius: `${BorderRadius}px`,
                    background: '#f5a524',
                    color: 'white',
                    fontFamily: 'revert-layer'
                },
                duration: Duration,
                icon: '🥸'
            })
            return false;
        }
    }
    return true;
}

/**
 * params参数验证（返回结果）
 * @param params {...params}
 * @return (boolean | string)[]
 */
function validateParameterRes(params: Record<string, string | null | undefined> /*接受键值对*/): (boolean | string)[] {
    /*循环判断*/
    for (const paramName in params) {
        const param = params[paramName];
        if (param === undefined || param === null || param === '') {
            /*read参数名*/
            return [false, `参数 ${paramName} 不能留空`];
        }
    }
    return [true, ''];
}

export {
    validateParameter, validateParameterRes, validate
}