import React, {useEffect} from "react";
import {Switch, useSwitch, VisuallyHidden, SwitchProps} from "@nextui-org/react";
import {useTheme} from "next-themes";
import {LuSunDim} from "react-icons/lu";
import {GrMoon} from "react-icons/gr";

const ThemeSwitch = (props: SwitchProps) => {
    /**
     * 主题切换
     */
    const {theme, setTheme} = useTheme()
    const {
        Component,
        slots,
        isSelected,
        getBaseProps,
        getInputProps,
        getWrapperProps
    } = useSwitch(props);

    useEffect(() => {
            return () => {
                isSelected ? setTheme('dark') : setTheme('light')
            };
        },
        [isSelected, setTheme]);


    return (
        <div className="flex flex-col gap-2">
            <Component {...getBaseProps()}>
                <VisuallyHidden>
                    <input {...getInputProps()} />
                </VisuallyHidden>
                <div
                    {...getWrapperProps()}
                    className={slots.wrapper({
                        class: [
                            "w-8 h-8",
                            "flex items-center justify-center",
                            "rounded-lg bg-default-100 hover:bg-default-200",
                        ],
                    })}
                >
                    {isSelected ? <LuSunDim/> : <GrMoon/>}
                </div>
            </Component>
        </div>
    )
}


export default function ThemeSwitchCom() {
    return <ThemeSwitch/>
}
