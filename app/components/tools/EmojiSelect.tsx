import {Modal, ModalBody, ModalContent, Radio, RadioGroup} from "@nextui-org/react";
import React, {useState} from "react";
import EmojiPicker, {EmojiClickData, EmojiStyle, Theme} from "emoji-picker-react";
import {useTheme} from "next-themes";

export default function EmojiSelect({isSelect, onSelectChange, setEmoji}: any) {
    const {theme} = useTheme()
    const [selected, setSelected] = useState(EmojiStyle.NATIVE);
    return (
        <Modal
            size={'sm'}
            backdrop="transparent"
            placement={"auto"}
            isOpen={isSelect}
            onOpenChange={onSelectChange}
            hideCloseButton={false}
        >
            <ModalContent>
                {() => (
                    <ModalBody className={`flex text-center items-center justify-center pt-10`}>
                        {/*<RadioGroup
                            label="选择表情表类别呀~✌️"
                            orientation="horizontal"
                            size={'sm'}
                            value={selected}
                            // @ts-ignore
                            onValueChange={setSelected}
                        >
                            <Radio value={EmojiStyle.NATIVE}>Native</Radio>
                            <Radio value={EmojiStyle.APPLE}>Apple</Radio>
                            <Radio value={EmojiStyle.TWITTER}>Twitter</Radio>
                            <Radio value={EmojiStyle.GOOGLE}>Google</Radio>
                        </RadioGroup>*/}
                        {/*表情选择器*/}
                        <EmojiPicker emojiStyle={selected}
                                     theme={theme == 'dark' ? Theme.DARK : Theme.LIGHT}
                                     onEmojiClick={(emojiData: EmojiClickData) => {
                                         setEmoji(emojiData.emoji)
                                     }}
                        />
                    </ModalBody>
                )}
            </ModalContent>
        </Modal>
    )
}