import {Button, Link, NavbarContent, NavbarItem} from "@nextui-org/react";
import React from "react";
import PubSub from "pubsub-js"

/**
 * 没有登录时的右上角内容
 * @param onOpen
 * @constructor
 */
export default function UnAuthenticatedNavbarContent({onOpen}: any) {
    return (
        <NavbarContent justify="end">
            <NavbarItem className="lg:flex">
                <Button as={Link} color="primary" variant="flat" onClick={
                    onOpen
                }>
                    免费使用
                </Button>
            </NavbarItem>
        </NavbarContent>
    )
}