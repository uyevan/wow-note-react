import {Card, Code, Modal, ModalBody, ModalContent, ModalHeader, ModalProps, ScrollShadow} from "@nextui-org/react";
import AuthPage from "@/app/components/auth/authPgae";
import React from "react";
import Share from "@/app/pages/share/Share";
import {BrowserRouter as Router} from 'react-router-dom';
import {CardBody} from "@nextui-org/card";
import PubSub from "pubsub-js"

/**
 * 无登录Navbar选项提示Modal
 * @param isOpenGuestNavbarClick
 * @param onOpenChangeGuestNavbarClick
 * @param title
 * @param body
 * @constructor
 */
export default function GuestNavbarComponents({
                                                  isOpenGuestNavbarClick,
                                                  onOpenChangeGuestNavbarClick,
                                                  title,
                                                  body
                                              }: any) {

    return (
        <>
            <Modal backdrop={"blur"}
                   placement={"auto"}
                   size={"5xl"}
                   isOpen={isOpenGuestNavbarClick}
                   onOpenChange={onOpenChangeGuestNavbarClick}
                   scrollBehavior={'inside'}
            >
                <ModalContent>
                    {onClose => (
                        <>
                            {/*头部*/}
                            <ModalHeader
                                className={"flex flex-col gap-1 font-extrabold dark:text-white light:text-black "}>{title}</ModalHeader>
                            {/*内容*/}
                            <ModalBody className={`scroll-m-28 p-0 pe-0`}>
                                {/*{body}*/}
                                <ScrollShadow className={`rounded-2xl`}
                                              orientation={'vertical'} hideScrollBar={false} size={20}>
                                    {body}
                                </ScrollShadow>
                            </ModalBody>
                        </>
                    )}
                </ModalContent>
            </Modal></>
    )
}