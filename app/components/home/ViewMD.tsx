import {MdPreview} from "md-editor-rt";
import {useState} from "react";
import {OVER} from "@/app/assets/data/over";
import {ABOUT} from "@/app/assets/data/about";
import {HELP} from "@/app/assets/data/help";
import {useTheme} from "next-themes";

/**
 * 预览MD
 * @param source
 * @constructor
 */
export default function ViewMD({source}: any) {
    /**
     * 主题切换
     */
    const {theme, setTheme} = useTheme();
    return (
        <>
            <MdPreview
                modelValue={source === 1 ? ABOUT : source === 2 ? HELP : OVER}
                previewTheme={'vuepress'}
                theme={theme == 'dark' ? 'dark' : 'light'}/>
        </>
    )
}