import {Button, Card, Image, Spacer} from "@nextui-org/react";
import React from "react";
import {TypeAnimation} from "react-type-animation";
import 'typeface-quicksand';
import {Quicksand} from "next/dist/compiled/@next/font/dist/google";
import {Divider} from "@nextui-org/divider";
import {AiFillRightCircle} from "react-icons/ai";
import {CardBody} from "@nextui-org/card";
// @ts-ignore
import {UndrawCoding, UndrawCreationProcess} from 'react-undraw-illustrations';


/**
 * 首页Body内容
 * @constructor
 */
export default function HomeBody({onOpen}: any) {
    return (
        <div
            className="h-full w-full overflow-auto pt-8 ps-0 pe-0 flex flex-col justify-start items-center dark:bg-default-200 bg-white MSpace">
            <TypeAnimation
                className="text-2xl md:text-5xl leading-tight md:leading-tight whitespace-pre-line font-bold dark:text-white light:text-black text-center"
                speed={50}
                repeat={0}
                sequence={[
                    '',
                    1000,
                    '您的📕百事百科',
                    1000,
                    '您的📕百事百科，📒云储文档',
                    1000,
                    '您的📕百事百科，📒云储文档，\n&🎯项目。',
                    1000,
                    '您的📕百事百科，📒云储文档，\n&🎯项目。共享合作。',
                    1000,
                ]}
            />
            <Spacer y={3}/>
            <div className="font-extralight text-xs md:text-1xl dark:text-white light:text-black text-center">
                <span>个人开发的基于微服务方案的一款个人云储笔记项目，</span>
                <Spacer y={0}/>
                <span>数字化信息管理的一站式工作中心。</span>
            </div>
            <Spacer y={8}/>
            <Button
                className={'bg-black text-white rounded-[3px] h-16 font-light shadow-lg dark:bg-white dark:text-black'}
                size={'md'}
                endContent={<AiFillRightCircle/>}
                onPress={onOpen}>
                立 即 创 建
            </Button>
            <Spacer y={12}/>
            <div className={'flex w-5/6 justify-around items-center h-[80px] px-5'}>
                <UndrawCreationProcess
                    skinColor="#FFF"
                    primaryColor="#000"
                    accentColor="#000"
                    hairColor="#000"
                    height="100px"
                />
                <UndrawCoding
                    skinColor="#FFF"
                    primaryColor="#FFF"
                    accentColor="#000"
                    hairColor="#000"
                    height="100px"
                />
            </div>
            <Card className="
            filter
            w-5/6
            md:w-3/6
            h-full
            rounded-t-lg
            overflow-hidden
            bg-cover
            bg-left-top
            border
            dark:border-gray-600
            bg-body-light
            dark:bg-body-dark"
                  radius="none"
                  shadow="lg"
            />
            <Spacer y={8}/>
            <div
                className="fixed bottom-0 left-0 w-full border-t-1 dark:border-t-gray-600 flex justify-center items-center bg-white dark:bg-default-200 h-16">
                <span className="text-xs md:text-sm font-light text-black dark:text-white">
                    ©2024 Evan . All rights reserved. 陕ICP备2022014030号-2
                </span>
            </div>
        </div>
    )
}


/**
 * Old main page.
 * <code>
 *     <div className={"flex flex-wrap p-8 justify-evenly bg-white dark:bg-default-200 items-center flex-grow"}>
 *             <h1 className={"font-extralight text-6xl MSpace dark:text-white light:text-black "}>下一代个人云笔记<br/>
 *                 <span className={"text-primary text-5xl font-bold mb-16 MSpace"}>&nbsp;Wow Note.🤯</span>
 *             </h1>
 *             <Image
 *                 src={"/assets/svg/MSpace.svg"}
 *                 alt="MSpace"
 *                 className={'MSpace'}
 *             />
 *         </div>
 * </code>
 *
 * Image Style.
 * <Image
 *                         className="w-full h-full rounded-t-3xl object-cover bg-auto"
 *                         radius="none"
 *                         shadow="none"
 *                         alt="Wow Note Image"
 *                         src="assets/image/demo.png"
 *                     />
 * Text Style.
 * <div className="text-5xl font-bold MSpace dark:text-white light:text-black text-center">
 *                 <span>你的📕百事百科,📒云储文档,</span>
 *                 <Spacer y={3}/>
 *                 <span>&🎯项目。以共享合作。</span>
 *             </div>
 */