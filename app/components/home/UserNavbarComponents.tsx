import {Card, Code, Modal, ModalBody, ModalContent, ModalHeader} from "@nextui-org/react";
import AuthPage from "@/app/components/auth/authPgae";
import React from "react";
import Share from "@/app/pages/share/Share";
import {BrowserRouter as Router} from 'react-router-dom';
import {CardBody} from "@nextui-org/card";
import PubSub from "pubsub-js"

export default function UserNavbarComponents({isOpenNavBarClick, onOpenChangeNavBarClick, title, message}: any) {
    return (
        <>
            <Modal backdrop={"blur"} isOpen={isOpenNavBarClick} onOpenChange={onOpenChangeNavBarClick}
                   placement={"auto"}
                   size={"5xl"}>
                <ModalContent>
                    {onClose => (
                        <>
                            {/*头部*/}
                            <ModalHeader
                                className={"flex flex-col gap-1 font-extrabold dark:text-white light:text-black "}>{title}</ModalHeader>
                            {/*内容*/}
                            <ModalBody className={'mb-4'}>
                                <div>
                                    <Card className={'h-1/3'} shadow={"md"} radius={'lg'}>
                                        <CardBody>
                                            <div className={'flex justify-center flex-grow items-center p-5'}>
                                                <Code color="warning"> {message} </Code>
                                            </div>
                                        </CardBody>
                                    </Card>
                                </div>
                            </ModalBody>
                        </>
                    )}
                </ModalContent>
            </Modal></>
    )
}