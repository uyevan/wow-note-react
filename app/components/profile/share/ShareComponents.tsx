import {Modal, ModalBody, ModalContent, ModalHeader, ScrollShadow} from "@nextui-org/react";
import React, {useEffect, useState} from "react";
import Share from "@/app/pages/share/Share";
import {BrowserRouter as Router} from 'react-router-dom';
import {state} from "sucrase/dist/types/parser/traverser/base";

export default function ShareComponents({isOpenShare, onOpenChangeShare}: any) {
    return (
        <>
            <Modal backdrop={"blur"}
                   isOpen={isOpenShare}
                   onOpenChange={onOpenChangeShare}
                   placement={"auto"}
                   size={"5xl"}
            >
                <ModalContent>
                    {onClose => (
                        <>
                            {/*头部*/}
                            <ModalHeader
                                className={"flex flex-col gap-1 font-extrabold dark:text-white light:text-black "}>我分享的笔记
                                🍧</ModalHeader>
                            {/*内容*/}
                            <ModalBody className={'mb-4'}>
                                <Router>
                                    <Share/>
                                </Router>
                            </ModalBody>
                        </>
                    )}
                </ModalContent>
            </Modal>
        </>
    )
}