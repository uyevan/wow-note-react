import {
    Button,
    Card,
    CardHeader,
    CardFooter,
    Divider,
    Link,
    Image,
    Modal,
    ModalBody,
    ModalContent,
    ModalFooter,
    ModalHeader, Code, Chip, Spacer
} from "@nextui-org/react";
import React from "react";
import {NoteIcon} from "@/app/assets/icons/NoteIcon";
import {CardBody} from "@nextui-org/card";

/**
 * 设置中心组件
 * @param isOpenSet
 * @param onOpenSetChange
 * @constructor
 */
export default function SettingPage({isOpenSet, onOpenSetChange}: any) {
    return (
        <Modal
            backdrop="blur"
            isOpen={isOpenSet}
            placement={'auto'}
            onOpenChange={onOpenSetChange}
        >
            <ModalContent>
                {(onClose) => (
                    <>
                        <ModalHeader className="flex flex-col gap-1 dark:text-white light:text-black ">设 置 中
                            心</ModalHeader>
                        <ModalBody>
                            {/*<div className={"flex justify-center flex-grow items-center pb-4"}>
                                <Code color="warning"> 这里是空空的~🍃 </Code>
                            </div>*/}
                            <Card className="w-full" shadow={'none'}>
                                <CardHeader className="flex gap-3">
                                    <NoteIcon/>
                                    <div className="flex flex-col">
                                        <p className="text-md">Wow Note</p>
                                        <p className="text-small text-default-500">note.jfkj.xyz</p>
                                    </div>
                                    <Chip variant={'dot'} color={"danger"} size={'sm'}>v2.0.0</Chip>
                                </CardHeader>
                                <Divider/>
                                <CardBody>
                                    <p>&nbsp;开源、免费的笔记云储平台🤪。以数字化的方式存储与共享文档、使得信息流向正确的方向走.✌️</p>
                                </CardBody>
                                <Divider/>
                                <Spacer y={3}/>
                                <Image
                                    width={160}
                                    alt="Me Wechat."
                                    src="/assets/image/me_wechat.png"
                                />
                                <CardFooter>
                                    <Link
                                        isExternal
                                        showAnchorIcon
                                        href="https://jfkj.xyz"
                                    >
                                        访问我的主页查看更多。
                                    </Link>
                                </CardFooter>
                            </Card>
                        </ModalBody>
                    </>
                )}
            </ModalContent>
        </Modal>
    )
}