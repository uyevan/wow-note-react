import {
    Button, Chip, Input,
    Modal,
    ModalBody,
    ModalContent,
    ModalFooter,
    ModalHeader,
    Textarea,
    useDisclosure
} from "@nextui-org/react";
import React, {useEffect, useState} from "react";
import NoteContent from "@/app/service/noteContent";
import {MdEditor, ToolbarTips} from 'md-editor-rt';
import 'md-editor-rt/lib/style.css';
import UpdateNote from "@/app/service/UpdateNote";
import {decodeFromBase64, encodeToBase64} from "next/dist/build/webpack/loaders/utils";
import useRequest from "@/app/utils/axiosRequest";
import {response} from "express";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";
import ToastUtil from "@/app/utils/toastUtil";
import UploadFileService from "@/app/service/UploadFileService";
import EmojiPicker from "emoji-picker-react";
import EmojiSelect from "@/app/components/tools/EmojiSelect";
import {useTheme} from "next-themes";

export default function CreateNote({isOpenNote, onOpenChangeNote, folderId}: any) {
    /*获取的数据 -- 标题*/
    const [title, setTitle] = useState('');
    /*获取的数据 -- 内容*/
    const [content, setContent] = useState('');
    /*获取的数据 -- Emoji*/
    const [emoji, setEmoji] = useState('📄');
    /*选择表情*/
    const {isOpen: isSelectEmoji, onOpen: onOpenSelectEmoji, onOpenChange: onSelectChange} = useDisclosure()
    /*编辑器状态栏*/
    const [toolbars] = useState<(keyof ToolbarTips)[] | undefined>(
        [
            'save', /*底部按钮保存 */
            'bold',
            'underline',
            'italic',
            '-',
            'strikeThrough',
            'title',
            'sub',
            'sup',
            'quote',
            'unorderedList',
            'orderedList',
            '-',
            'codeRow',
            'code',
            'link',
            'image',
            'task',
            '-',
            'table',
            'mermaid',
            'katex',
            '-',
            'revoke',
            'next',
            '=',
            'pageFullscreen',
            'preview',
            'htmlPreview',
            'prettier',
            'catalog'
        ],
    );

    /**
     * 主题切换
     */
    const {theme, setTheme} = useTheme()
    /*引用 axios 封装主键*/
    const axiosInstance = useRequest();
    /*null是指空 undefined是指不存在*/
    folderId = folderId === null ? 'root' : folderId;
    /*创建笔记*/
    const createNote = async () => {
        axiosInstance.post('/note/note/recordNote', {
            "folderId": folderId, /*父级目录ID*/
            "title": title,
            "icon": emoji,
            'content': encodeToBase64(content),
            "tagId": 'TagId',
            "username": decodeFromBase64(cookie.load(UserName))
        }).then(response => {
            const data = response.data;
            if (data.status) {
                ToastUtil(data.message, '🥰', "success")
                /*发布一个消息 用于更新列表*/
                PubSub.publish('refreshNote', true);
                onOpenChangeNote()
                /*设置空值*/
                setContent('');
                setTitle('');

            } else {
                ToastUtil(data.message, '😰', "error")
            }
        }).catch(error => {
            ToastUtil(error.message, '🥸', "error")
        });
    }

    return (
        <>
            <Modal
                size={'full'}
                isOpen={isOpenNote}
                onOpenChange={onOpenChangeNote}
                backdrop={"blur"}
                isDismissable={false}
                radius={"lg"}
                placement={"auto"}
                scrollBehavior={'inside'}
                className={`h-full`}
            >
                <ModalContent>
                    {(onClose) => (
                        <>
                            <ModalHeader className="flex flex-col gap-1 dark:text-white light:text-black ">新 建 笔 记</ModalHeader>
                            <ModalBody>
                                <div className={'flex text-center items-center'}>
                                    {/*Icon*/}
                                    <div className={'flex w-[170px] text-center justify-center items-center'}>
                                        <p className={'text-[12px] text-center dark:text-white light:text-black '}>✌️ Emoji：</p>
                                        <p className={`text-3xl shadow border rounded-md pt-0.5 pb-0.5`}
                                           onClick={(v) => {
                                               onOpenSelectEmoji()
                                           }}>{emoji}</p>
                                    </div>
                                    {/*标题*/}
                                    <Input
                                        size={'sm'}
                                        fullWidth={true}
                                        label={'🔖 笔记名：'}
                                        labelPlacement={"outside-left"}
                                        variant="bordered"
                                        value={title}
                                        className={"w-full dark:text-white light:text-black "}
                                        onValueChange={setTitle}
                                    />
                                </div>
                                {/*内容*/}
                                <MdEditor className={'rounded-lg'}
                                          modelValue={content}
                                          onChange={setContent}
                                          style={
                                              {
                                                  height: '100%'
                                              } as React.CSSProperties}
                                          toolbars={toolbars}
                                          theme={theme == 'light' ? 'light' : 'dark'}
                                          showCodeRowNumber={true}
                                          noUploadImg={false}
                                          autoDetectCode={true}
                                          preview={true}
                                          showToolbarName
                                          onUploadImg={(files, callback) => {
                                              UploadFileService(files, callback).then(r => {
                                              })
                                          }}
                                          onSave={() => {/*保存事件 小图标有效 还有快捷键*/
                                              /*创建笔记*/
                                              createNote().then(r => {
                                              })
                                          }}
                                    /*onHtmlChanged={(h) => {console.log(h)}}*/
                                />
                            </ModalBody>
                            <ModalFooter>
                                <Button size={"md"} color="danger" variant="light" onPress={onClose}>
                                    取 消
                                </Button>
                                <Button size={"md"} color="primary" onClick={() => {
                                    /*创建笔记*/
                                    createNote().then(r => {
                                    })
                                }}>
                                    创 建
                                </Button>
                            </ModalFooter>
                        </>
                    )}
                </ModalContent>
            </Modal>

            {/*表情选择*/}
            <EmojiSelect isSelect={isSelectEmoji} onSelectChange={onSelectChange} setEmoji={setEmoji}/>
        </>
    )
}