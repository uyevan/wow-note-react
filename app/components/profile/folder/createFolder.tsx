import {
    Button,
    Input,
    Modal,
    ModalBody,
    ModalContent,
    ModalFooter,
    ModalHeader,
    Textarea,
    useDisclosure
} from "@nextui-org/react";
import React, {useState} from "react";
import {BiFolderOpen, BiLabel} from "react-icons/bi";
import useRequest from "@/app/utils/axiosRequest";
import {decodeFromBase64} from "next/dist/build/webpack/loaders/utils";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";
import ToastUtil from "@/app/utils/toastUtil";
import EmojiSelect from "@/app/components/tools/EmojiSelect";

/**
 * 创建新的文件夹
 * @param isOpenFolder
 * @param onOpenChangeFolder
 * @param parentId
 * @constructor
 */
export default function CreateFolder({isOpenFolder, onOpenChangeFolder, parentId}: any) {
    //axios接口
    const axiosInstance = useRequest();
    /*获取的数据 -- Emoji*/
    const [emoji, setEmoji] = useState('📂');
    /*选择表情*/
    const {isOpen: isSelectEmoji, onOpen: onOpenSelectEmoji, onOpenChange: onSelectChange} = useDisclosure()
    /*定义目录名和简介状态*/
    const [folderData, setFolderData] = useState({
        title: '',
        content: ''
    });
    //空判断
    parentId = parentId === null ? 'root' : parentId;
    const createFolder = async () => {
        await axiosInstance.post("/note/folder/createFolder", {
            "parentId": parentId, /*父级目录ID*/
            "folderTitle": folderData.title,
            "icon": emoji,
            'folderContent': folderData.content,
            "username": decodeFromBase64(cookie.load(UserName))
        }).then(response => {
            const data = response.data;
            if (data.status) {
                ToastUtil(data.message, '🥰', "success")
                /*发布消息更新文件夹列表*/
                PubSub.publish('refreshFolder', true);
                onOpenChangeFolder()
            } else {
                ToastUtil(data.message, '😰', "error")
            }
        }).catch(error => {
            ToastUtil(error.message, '🥸', "error")
        });
    }
    /*返回组件*/
    return (
        <>
            <Modal backdrop={"blur"} isOpen={isOpenFolder} onOpenChange={onOpenChangeFolder}
                   placement={"auto"}
                   isDismissable={false}>
                <ModalContent>
                    {
                        onClose => (
                            <>
                                <ModalHeader className={`dark:text-white light:text-black `}>创建文件夹
                                    📂</ModalHeader>
                                <ModalBody>
                                    {/*Icon*/}
                                    <div className={'flex text-center justify-start items-start pb-1'}>
                                        <p className={`text-3xl shadow border rounded-md pt-0.5 pb-0.5`}
                                           onClick={(v) => {
                                               onOpenSelectEmoji()
                                           }}>{emoji}</p>
                                    </div>
                                    <Input
                                        type="text"
                                        className={`dark:text-white light:text-black `}
                                        label="目 录 名"
                                        variant={"faded"}
                                        labelPlacement="outside"
                                        onValueChange={(d) => {
                                            setFolderData({
                                                ...folderData,
                                                title: d
                                            })
                                        }}
                                        startContent={
                                            <BiFolderOpen
                                                className="text-2xl text-default-300 pointer-events-none flex-shrink-0"/>
                                        }
                                    />
                                    <Textarea
                                        className={"break-all dark:text-white light:text-black "}
                                        type="text"
                                        label="简 介"
                                        variant={"faded"}
                                        labelPlacement="outside"
                                        color={"default"}
                                        onValueChange={(d) => {
                                            setFolderData({
                                                ...folderData,
                                                content: d
                                            })
                                        }}
                                        startContent={
                                            <BiLabel
                                                className="text-2xl text-default-300 pointer-events-none flex-shrink-0"/>
                                        }
                                    />
                                </ModalBody>
                                <ModalFooter>
                                    <Button className={"w-1/3"} variant={"shadow"} color="primary"
                                            onClick={createFolder}>
                                        创 建 ❤️
                                    </Button>
                                </ModalFooter>
                            </>
                        )
                    }
                </ModalContent>
            </Modal>
            {/*表情选择*/}
            <EmojiSelect isSelect={isSelectEmoji} onSelectChange={onSelectChange} setEmoji={setEmoji}/>
        </>
    )
}