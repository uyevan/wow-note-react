import {Button, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader} from "@nextui-org/react";
import cookie from "react-cookies";
import {Token, UserName} from "@/app/constants/authTokenConstants";
import React from "react";
import PubSub from "pubsub-js"

/**
 * 退出登录组件
 * @param isExit
 * @param setExit
 * @constructor
 */
export default function ExitLogin({isExit, setExit}: any) {
    return (
        <Modal
            backdrop="blur"
            placement={'auto'}
            isOpen={isExit}
            onOpenChange={setExit}
            hideCloseButton={true}
        >
            <ModalContent>
                {(onClose) => (
                    <>
                        <ModalHeader className="flex flex-col gap-1 dark:text-white light:text-black ">退 出 登 录</ModalHeader>
                        <ModalBody>
                            <p className={`dark:text-white light:text-black `}>
                                你确定退出登录嘛？不要嘛🥹!
                            </p>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" variant={"flat"} onPress={onClose}>
                                取 消
                            </Button>
                            <Button color="danger" variant="flat" onPress={() => {
                                /*移除Cookie*/
                                cookie.remove(UserName);
                                cookie.remove(Token);
                                window.location.reload()
                            }}>
                                确 定 退 出
                            </Button>
                        </ModalFooter>
                    </>
                )}
            </ModalContent>
        </Modal>
    )
}