'use client'
import {NextUIProvider} from '@nextui-org/react'
import {ThemeProvider as NextThemesProvider} from "next-themes";
import React from "react";

/**
 * 套入全局组件到NexUI主体
 * @param children
 * @constructor
 */
export function Providers({children}: { children: React.ReactNode }) {
    return (
        /*NextUI根Provider*/
        <NextUIProvider>
            <NextThemesProvider attribute="class" defaultTheme="light">
                {children}
            </NextThemesProvider>
        </NextUIProvider>
    )
}