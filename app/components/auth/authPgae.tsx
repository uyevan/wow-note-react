/*注册登录*/
import { Tab, Tabs } from "@nextui-org/react"
import React, { useEffect, useState } from "react"
import Login from "@/app/pages/auth/login"
import Register from "@/app/pages/auth/register"
import PubSub from "pubsub-js"

/**
 * 登录和注册子主体
 * @param props
 * @constructor
 */
export default function AuthPage(props: any) {

  const { isLogin } = props
  const [login, setLogin] = useState(isLogin)

  /*更换到Login 接受消息(pub-sub js库)*/
  PubSub.unsubscribe("isLogin")
  PubSub.subscribe("isLogin", (_, data) => {
    setLogin(data)
    /*/接受完取消订阅*/
    PubSub.unsubscribe("isLogin")
  })

  /*账号和密码消息*/
  PubSub.unsubscribe("username")/*接触订阅*/
  PubSub.subscribe("username", (_, data) => {
    PubSub.publish("username", data)
  })

  PubSub.unsubscribe("password")/*接触订阅*/
  PubSub.subscribe("password", (_, data) => {
    PubSub.publish("password", data)
  })


  return (
    <>
      <Tabs fullWidth={true} size={'md'} color={"primary"} aria-label={"auth page"}
            selectedKey={login ? "login" : "register"} onSelectionChange={() => setLogin(!login)}>
        <Tab key={"login"} title={"登 录"}><Login /></Tab>
        <Tab key={"register"} title={"注 册"}><Register /></Tab>
      </Tabs>
    </>
  )
}