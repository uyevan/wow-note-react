import {Modal, ModalBody, ModalContent, ModalHeader} from "@nextui-org/react";
import AuthPage from "@/app/components/auth/authPgae";
import React from "react";

/**
 * 登录和注册的弹窗主体
 * @param isOpen
 * @param onOpenChange
 * @constructor
 */
export default function AuthModal({isOpen, onOpenChange}: any) {
    return (
        <Modal backdrop={"blur"} isOpen={isOpen} onOpenChange={onOpenChange}
               placement={"auto"}
               size={"md"}>
            <ModalContent>
                {onClose => (
                    <>
                        {/*头部*/}
                        <ModalHeader className={"flex flex-col gap-1 font-extrabold dark:text-white light:text-black "}>Wow
                            Note
                            认证</ModalHeader>
                        {/*内容*/}
                        <ModalBody>
                            <AuthPage isLogin={true}/>
                        </ModalBody>
                        {/*底部*/}
                        {/*<ModalFooter>
                                <Button color={"default"} variant={"flat"} onPress={onClose}>取消</Button>
                                <Button color={"primary"} onPress={onClose}>确定</Button>
                            </ModalFooter>*/}
                    </>
                )}
            </ModalContent>
        </Modal>
    )
}