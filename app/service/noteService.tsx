import useRequest from "@/app/utils/axiosRequest";
import {decodeFromBase64, encodeToBase64} from "next/dist/build/webpack/loaders/utils";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";
import {Simulate} from "react-dom/test-utils";
import axios from "axios";
import ToastUtil from "@/app/utils/toastUtil";

/**
 * 获取笔记列表
 * @param folderId
 * @constructor
 */
export default async function NoteService(folderId: string) {
    //axios统一配置中心
    const axiosInstance = useRequest();
    /*获取笔记目录*/
    return await axiosInstance.get("/note/note/listNotes", {
        params: {
            "folderId": folderId, /*目录ID*/
            "username": decodeFromBase64(cookie.load(UserName))
        }
    }).then(response => {
        /*const data = response.data;
        if (data.status) ToastUtil(data.message, '🤓', "success")
        else ToastUtil(data.message, '🤕', "error")*/
        return response.data;
    }).catch(error => {
        return error.data;
    });
}