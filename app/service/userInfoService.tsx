import cookie from "react-cookies";
import {Token, UserName} from "@/app/constants/authTokenConstants";
import {validate} from "@/app/utils/validation";
import {decodeFromBase64} from "next/dist/build/webpack/loaders/utils";
import useRequest from "@/app/utils/axiosRequest";
import ToastUtil from "@/app/utils/toastUtil";

/**
 * 个人信息获取
 * @param setEmail
 * @param setCreateTime
 * @param setNickname
 * @param setAvatar
 * @param setNickname
 * @param setAvatar
 * @constructor
 */
export default async function UserInfoService({setEmail, setCreateTime, setNickname, setAvatar}: any) {

    //axios统一配置中心
    const axiosInstance = useRequest();

    if (validate(cookie.load(Token)) || validate(cookie.load(UserName))) {
        return false;
    }

    await axiosInstance.get('/user/user/info', {
        params: {
            'username': decodeFromBase64(cookie.load(UserName))
        }
    }).then(response => {
        /*获取返回信息*/
        if (response.status) {
            const data = response.data;
            //成功拿到了个人信息
            setEmail(data.data.email)
            setCreateTime(data.data.createTime)
            setAvatar(data.data.avatar)
            setNickname(data.data.nickname)
            return true;
        }
        setEmail(response.data.message)
        setCreateTime(response.data.message)
        setAvatar('')
        setNickname(response.data.message)
        return false;
    }).catch(error => {
        ToastUtil(error.message, '🤒', "error")
        return false;
    })
}