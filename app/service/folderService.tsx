import useRequest from "@/app/utils/axiosRequest";
import {decodeFromBase64} from "next/dist/build/webpack/loaders/utils";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";

/**
 * 获取文件夹列表
 * @param parentId
 * @constructor
 */
export default async function FolderService(parentId: string) {
    //axios统一配置中心
    const axiosInstance = useRequest();
    /*获取目录数据*/
    return await axiosInstance.get("/note/folder/listFolders", {
        params: {
            "parentId": parentId, /*父级目录ID*/
            "username": decodeFromBase64(cookie.load(UserName))
        }
    }).then(response => {
        const data = response.data;
        /*if (data.status) ToastUtil(data.message, '😇', "success")
        else ToastUtil(data.message, '😓', "error")*/
        return response.data;
    }).catch(error => {
        return error.data;
    });

}