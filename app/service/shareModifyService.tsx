import useRequest from "@/app/utils/axiosRequest";
import ToastUtil from "@/app/utils/toastUtil";
import {decodeFromBase64} from "next/dist/build/webpack/loaders/utils";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";


/**
 * 获取笔记列表
 * @param shareId
 * @param type
 * @param refreshType
 * @constructor
 */
export default async function ShareModifyService(shareId: string, type: number, refreshType: (number | 0 | 1)): Promise<boolean> {
    //axios统一配置中心
    const axiosInstance = useRequest();
    /*获取笔记目录*/
    return await axiosInstance.put("/share/share/modify", {
        "shareId": shareId, /*目录ID*/
        "type": type,
        "username": decodeFromBase64(cookie.load(UserName))
    }).then(response => {
        const data = response.data;
        if (!data.status) {
            ToastUtil(data.message, '❎', "error")
        } else {
            ToastUtil(data.message, '🤙', "success")
            if (refreshType === 1) {
                PubSub.publish("refreshShare", true)
            } else if (refreshType === 0) {
                PubSub.publish("refreshNote", true)
            } else {
                PubSub.publish("refreshShare", true)
                PubSub.publish("refreshNote", true)
            }
            return true;
        }
        return false;
    }).catch(error => {
        return false;
    });
}