import useRequest from "@/app/utils/axiosRequest";
import {decodeFromBase64, encodeToBase64} from "next/dist/build/webpack/loaders/utils";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";

/**
 * 查找笔记
 * @param keyword
 * @constructor
 */
export default async function FindNoteService(keyword: string) {
    //axios统一配置中心
    const axiosInstance = useRequest();
    /*获取笔记目录*/
    return await axiosInstance.get("/note/note/findNotes", {
        params: {
            "keyword": encodeToBase64(keyword), /*目录ID*/
            "username": decodeFromBase64(cookie.load(UserName))
        }
    }).then(response => {
        return response.data;
    }).catch(error => {
        return error.data;
    });
}