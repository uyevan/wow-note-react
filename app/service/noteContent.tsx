import {decodeFromBase64} from "next/dist/build/webpack/loaders/utils";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";
import useRequest from "@/app/utils/axiosRequest";
import ToastUtil from "@/app/utils/toastUtil";

/**
 * 获取笔记内容
 * @param noteId
 * @constructor
 */
export default async function NoteContent(noteId: string) {
    //axios统一配置中心
    const axiosInstance = useRequest();
    if (noteId === undefined || noteId === null || noteId === '') {
        return false;
    }
    /*获取笔记内容*/
    return await axiosInstance.get("/note/note/viewNote", {
        params: {
            "noteId": noteId, /*笔记ID*/
            "username": decodeFromBase64(cookie.load(UserName))
        }
    }).then(response => {
        const data = response.data;
        if (data.status) {
            /*ToastUtil(data.message, '😙', "success")*/
            return data.data;
        }
        ToastUtil(data.message, '😒', "error")
        return false;
    }).catch(error => {
        ToastUtil(error.message, '🤒', "error")
        return false;
    });
}