import useRequest from "@/app/utils/axiosRequest";
import {decodeFromBase64, encodeToBase64} from "next/dist/build/webpack/loaders/utils";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";
import {Toaster} from "react-hot-toast";
import ToastUtil from "@/app/utils/toastUtil";
import 'react-toastify/dist/ReactToastify.css';
import {toast} from "react-toastify";
import PubSub from "pubsub-js"


/*参数名和调用的参数名必须相同*/
/**
 * 更新笔记
 * @param folderId
 * @param noteId
 * @param title
 * @param editorValue
 * @param setEditValue
 * @constructor
 */
export default async function UpdateNote({folderId, noteId, title, editorValue, emoji}: any) {
    /*console.log(`folderId: ${folderId}`);
    console.log('noteId: ' + noteId);
    console.log('title: ' + title);
    console.log('content: ' + editorValue);*/
    folderId = folderId === null ? 'root' : folderId;
    //axios统一配置中心
    const axiosInstance = useRequest();

    /*获取笔记目录*/
    return await axiosInstance.put("/note/note/updateNote", {
        "folderId": folderId,/*目录ID*/
        'noteId': noteId,/*笔记ID*/
        'tagId': 'TagID',/*标签ID*/
        'icon': emoji,
        'title': title,
        'content': encodeToBase64(editorValue),/*base64编码*/
        "username": decodeFromBase64(cookie.load(UserName))
    }).then(response => {
        const data = response.data;
        if (data.status) ToastUtil(data.message, '😎', "success");
        else ToastUtil(data.message, '😫', "error")
        return response.data;
    }).catch(error => {
        ToastUtil(error.message, '🤒', "error")
        return error.data;
    });
}