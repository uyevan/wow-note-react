import useRequest from "@/app/utils/axiosRequest";
import {decodeFromBase64} from "next/dist/build/webpack/loaders/utils";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";
import ToastUtil from "@/app/utils/toastUtil";
import {ModalFooter, useDisclosure} from "@nextui-org/react";
import {Modal, ModalContent, ModalHeader, ModalBody, Button} from "@nextui-org/react";

/**
 * 删除分享
 * @param shareId
 * @param isOpenDelShare
 * @param onOpenChangeDelShare
 * @constructor
 */
export default function DelShareService({
                                            isOpenDelShare: isOpenDelShare,
                                            onOpenChangeDelShare: onOpenChangeDelShare,
                                            shareId: shareId
                                        }: any) {
    /*引入axios*/
    const axiosInstance = useRequest();

    async function delShare() {
        await axiosInstance.delete('/share/share/delete', {
            params: {
                "shareId": shareId, /*分享ID*/
                "username": decodeFromBase64(cookie.load(UserName))
            }
        }).then(response => {
            const data = response.data;
            if (data.status) {
                ToastUtil(data.message, '🤙', "success")
                /*发布一个消息 用于更新列表*/
                onOpenChangeDelShare(false);
                PubSub.publish('refreshShare', true);
                PubSub.publish('refreshNote', true);
                return true;
            }
            ToastUtil(data.message, '😰', "error")
        }).catch(error => {
            ToastUtil(error.message, '🤒', "error")
        })
    }

    return (
        <>
            <Modal isOpen={isOpenDelShare} onOpenChange={onOpenChangeDelShare} isDismissable={false} backdrop={"blur"}>
                <ModalContent>
                    {(onClose) => (
                        <>
                            <ModalHeader className="flex flex-col gap-1 dark:text-white light:text-black ">删除笔记分享 👀</ModalHeader>
                            <ModalBody>
                                <p className={`dark:text-white light:text-black `}>
                                    确定删除该分享吗？一旦确认删除将无法恢复？🗑️
                                </p>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="primary" onPress={onClose}>
                                    取 消
                                </Button>
                                <Button color="danger" onPress={delShare} variant={"light"}>
                                    删 除
                                </Button>
                            </ModalFooter>
                        </>
                    )}
                </ModalContent>
            </Modal>
        </>
    )
}