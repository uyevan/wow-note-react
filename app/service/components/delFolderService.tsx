import useRequest from "@/app/utils/axiosRequest";
import {decodeFromBase64} from "next/dist/build/webpack/loaders/utils";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";
import ToastUtil from "@/app/utils/toastUtil";
import {useDisclosure} from "@nextui-org/react";
import {Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, Button} from "@nextui-org/react";

/**
 * 删除文件夹
 * @param isOpenDelFolder
 * @param onOpenChangeDelFolder
 * @param folderId
 * @constructor
 */
export default function DelFolderService({isOpenDelFolder, onOpenChangeDelFolder, folderId}: any) {
    /*引入axios*/
    const axiosInstance = useRequest();

    folderId = folderId === null ? 'root' : folderId;

    async function delFolder() {
        await axiosInstance.delete('/note/folder/deleteFolder', {
            params: {
                "folderId": folderId, /*笔记ID*/
                "username": decodeFromBase64(cookie.load(UserName))
            }
        }).then(response => {
            const data = response.data;
            if (data.status) {
                ToastUtil(data.message, '🥰', "success")
                /*发布一个消息 用于更新列表*/
                PubSub.publish('refreshFolder', true);
                onOpenChangeDelFolder(false);
                return true;
            }
            ToastUtil(data.message, '😰', "error")
        }).catch(error => {
            ToastUtil(error.message, '🤒', "error")
        })
    }

    return (
        <>
            <Modal isOpen={isOpenDelFolder} onOpenChange={onOpenChangeDelFolder} isDismissable={false}
                   backdrop={"blur"}>
                <ModalContent>
                    {(onClose) => (
                        <>
                            <ModalHeader className="flex flex-col gap-1 dark:text-white light:text-black ">删除文件夹 🚫</ModalHeader>
                            <ModalBody>
                                <p className={`dark:text-white light:text-black `}>
                                    确定删除该文件夹以及里面的所有文件夹和笔记吗？一旦确认删除将无法恢复？请谨慎操作！🥸
                                </p>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="primary" onPress={onClose}>
                                    取 消
                                </Button>
                                <Button color="danger" onPress={delFolder} variant={"light"}>
                                    删 除
                                </Button>
                            </ModalFooter>
                        </>
                    )}
                </ModalContent>
            </Modal>
        </>
    )
}