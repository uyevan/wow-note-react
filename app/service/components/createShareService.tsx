import useRequest from "@/app/utils/axiosRequest";
import {validateParameter} from "@/app/utils/validation";
import {response} from "express";
import {decodeFromBase64} from "next/dist/build/webpack/loaders/utils";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";
import moment from "moment";
import ToastUtil from "@/app/utils/toastUtil";
import {Button, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, Select, SelectItem} from "@nextui-org/react";
import React, {useState} from "react";
import {number} from "prop-types";

/**
 * 创建新的分享
 * @param isOpenShare
 * @param onOpenChangeShare
 * @param noteId
 * @constructor
 */
export default function CreateShareService({isOpenShare, onOpenChangeShare, noteId}: any) {
    const axiosInstance = useRequest();//初始化axios

    const [value, setValue] = useState<string>("10");//分享时间 默认10天
    /*指定*/
    const handleSelectionChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setValue(e.target.value);
    };
    //日期
    const animals = [
        {label: "1 天后", value: "1", description: "Share in 1 day."},
        {label: "5 天后", value: "5", description: "Share in 5 day."},
        {label: "10 天后", value: "10", description: "Share in 10 day."},/*默认十天*/
        {label: "20 天后", value: "20", description: "Share in 20 day."},
        {label: "30 天后", value: "30", description: "Share in 30 day."},

    ]

    async function create() {
        //创建
        await axiosInstance.post("/share/share/create", {
            endTime: moment().add(value, 'days').format('YYYY-MM-DD HH:mm:ss'),
            folderId: 0,
            noteId: noteId,
            username: decodeFromBase64(cookie.load(UserName))
        }, {headers: {"Content-Type": "application/json"}}).then((response) => {
            const data = response.data;
            if (!data.status) {
                ToastUtil(data.message, '😫', "error")
            } else {
                ToastUtil(data.message, '😎', "success");
                /*发布一个消息 用于更新列表*/
                PubSub.publish('refreshNote', true);
                onOpenChangeShare(false);
            }
            return;
        }).catch((error) => {
            ToastUtil(error.message, '🤒', "error")
        });
    }

    return (
        <>
            <Modal isOpen={isOpenShare} onOpenChange={onOpenChangeShare} isDismissable={true}
                   backdrop={"blur"}>
                <ModalContent>
                    {(onClose) => (
                        <>
                            <ModalHeader className="flex flex-col gap-1 dark:text-white light:text-black ">创建新分享
                                ✅</ModalHeader>
                            <ModalBody>
                                <Select
                                    isRequired
                                    label="请选择分享过期时间"
                                    placeholder="请选择分享过期时间"
                                    className={`dark:text-white light:text-black `}
                                    defaultSelectedKeys={["10"]}
                                    onChange={handleSelectionChange}
                                >
                                    {animals.map((animal) => (
                                        <SelectItem key={animal.value} value={animal.value}>
                                            {animal.label}
                                        </SelectItem>
                                    ))}
                                </Select>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="danger" onPress={onClose} variant={"light"}>
                                    取 消
                                </Button>
                                <Button color="primary" onPress={create}>
                                    分 享
                                </Button>
                            </ModalFooter>
                        </>
                    )}
                </ModalContent>
            </Modal>
        </>
    )

}