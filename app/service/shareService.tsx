import useRequest from "@/app/utils/axiosRequest";
import {response} from "express";
import {decodeFromBase64} from "next/dist/build/webpack/loaders/utils";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";

/**
 * 获取分享
 * @constructor
 */
export default async function ShareService() {
    const axiosInstance = useRequest();
    return await axiosInstance.get("/share/share/shares", {
        params: {
            "username": decodeFromBase64(cookie.load(UserName))
        }
    }).then(response => {
        if (response.status) {
            return response.data;
        }
        return response.data.message;
    }).catch(error => {
        return error.data;
    })
}