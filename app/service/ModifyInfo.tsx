import useRequest from "@/app/utils/axiosRequest";
import {decodeFromBase64} from "next/dist/build/webpack/loaders/utils";
import {Cookie} from "next/dist/compiled/@next/font/dist/google";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";
import ToastUtil from "@/app/utils/toastUtil";
import {response} from "express";

/**
 * 修改个人信息
 * @param nickname
 * @param avatar
 * @constructor
 */
export default async function ModifyInfo(nickname: string, avatar: string): Promise<boolean> {
    //axios请求对象
    const axiosInstance = useRequest();
    //封装数据
    const formData = new FormData();
    formData.append("nickname", nickname);
    formData.append("avatar", avatar);
    //请求
    return axiosInstance.post(
        `/user/user/modify?username=${decodeFromBase64(cookie.load(UserName))}`,
        formData,
        {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }
    )
        .then(response => {
            const data = response.data;
            if (data.status) {
                ToastUtil(data.message, '👌', "success");
                return true;
            }
            ToastUtil(data.message, '❌', "error");
            return false;
        })
        .catch(res => {
            ToastUtil("更新失败", '❌', "error");
            return false;
        })
}