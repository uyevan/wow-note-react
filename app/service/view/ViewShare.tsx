import useRequest from "@/app/utils/axiosRequest";
import {response} from "express";
import {decodeFromBase64} from "next/dist/build/webpack/loaders/utils";
import cookie from "react-cookies";
import {UserName} from "@/app/constants/authTokenConstants";
import ToastUtil from "@/app/utils/toastUtil";

/**
 * 获取分享
 * @constructor
 */
export default async function ViewShare(shareId: any) {
    const axiosInstance = useRequest();
    return await axiosInstance.get(`/share/view/${shareId}`).then(response => {
        return response.data;
    }).catch(error => {
        ToastUtil(error.data, '🤬', "error")
    });
}