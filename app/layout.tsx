import type {Metadata} from 'next'
import './assets/css/globals.css'
import * as React from "react";
import {Providers} from './components/providers';
import {Toaster} from "react-hot-toast";
import {Analytics} from '@vercel/analytics/react';
import {SpeedInsights} from '@vercel/speed-insights/next';


export const metadata: Metadata = {
    title: 'wow note',
    description: 'Useful Cloud Notes.',
}
/**
 * 全局根主体
 * @param children
 * @constructor
 */
export default function RootLayout({children}: { children: React.ReactNode }) {
    const theme = 'light'; /*light | dark*/
    return (
        <html lang="en" /*className={theme}*/>
        {/* suppressHydrationWarning 隐藏警告信息*/}
        <body suppressHydrationWarning={true}>
        {/*给所有page加NextUIProvider*/}
        <Providers>
            {children}
        </Providers>
        <Toaster/>
        <Analytics/>{/*访问监控*/}
        <SpeedInsights/>{/*性能监控*/}
        </body>
        </html>
    )
}
