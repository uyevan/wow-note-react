import CryptoJS from "crypto-js/core";
/*认证有关的常量*/
export let inFifteenMinutes = new Date(new Date().getTime() + 24 * 3600 * 1000 * 7); /*过期时间默认存储七天*/
export const Token = 'WN_TOKEN' /*token*/
export const UserName = 'WN_USERNAME' /*username*/
export const SECRET_KEY = CryptoJS.enc.Utf8.parse("#wownote20212255"); /*加密密钥*/