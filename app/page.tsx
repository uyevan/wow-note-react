/*文件共置（首页）*/
'use client'
import React, {useLayoutEffect, useState} from "react";
import {
    Navbar,
    NavbarBrand,
    NavbarMenuToggle,
    NavbarMenuItem,
    NavbarMenu,
    NavbarContent,
    NavbarItem,
    Link,
    useDisclosure,
    Spacer, Spinner, Code, Switch,
} from "@nextui-org/react";
import {NoteIcon} from "@/app/assets/icons/NoteIcon";
import {useRouter} from "next/navigation";
import Profile from "@/app/pages/profile/Profile";
import './assets/css/customStyle.css'
import OnAuthenticatedNavbarContent from "@/app/components/home/onAuthenticatedNavbarContent";
import UnAuthenticatedNavbarContent from "@/app/components/home/unAuthenticatedNavbarContent";
import AuthModal from "@/app/components/auth/authModal";
import ExitLogin from "@/app/components/profile/exitLogin";
import HomeBody from "@/app/components/home/homeBody";
import {AuthenticationChecker} from "@/app/config/authenticationChecker";
import {BrowserRouter as Router} from 'react-router-dom';
import ShareComponents from "@/app/components/profile/share/ShareComponents";
import UserNavbarComponents from "@/app/components/home/UserNavbarComponents";
import GuestNavbarComponents from "@/app/components/home/GuestNavbarComponents";
import ViewMD from "@/app/components/home/ViewMD";
import PubSub from 'pubsub-js'
import {MoonIcon, SunIcon} from "@nextui-org/shared-icons";
import theme from "tailwindcss/defaultTheme";
import {useTheme} from "next-themes";
import ThemeSwitchCom from "@/app/components/tools/ThemeSwitch";

/**
 * 入口页
 * @constructor
 */
export default function App() {
    /*路由*/
    const router = useRouter();

    /*Loading*/
    const [loading, setLoading] = useState(true);

    /*已登录过状态*/
    const [loginEd, setLoginEd] = useState(false);

    /*用户信息*/
    const [username, setUsername] = useState('');

    /*手机模式Menu状态*/
    const [isMenuOpen, setIsMenuOpen] = React.useState(false);

    /*顶部选项 --- 未登录*/
    const menuItems_unLogin = [
        "项目介绍",
        "帮助中心",
        "关于我们"
    ];

    /*顶部选项 --- 已登录*/
    const menuItems_inLogin = [
        "我的分享",
        "我的标签",
        "我的评论"
    ];

    const menuData = loginEd ? menuItems_inLogin : menuItems_unLogin;

    /*定义Dialog状态*/
    const {isOpen, onOpen, onOpenChange} = useDisclosure();
    const [isExit, setExit] = useState(false);
    /*Share State.*/
    const {
        isOpen: isOpenShare,
        onOpen: onOpenShare,
        onOpenChange: onOpenChangeShare
    } = useDisclosure()
    const {
        isOpen: isOpenNavbarClick,
        onOpen: onOpenNavbarClick,
        onOpenChange: onOpenChangeNavbarClick
    } = useDisclosure()
    const {
        isOpen: isOpenGuestNavbarClick,
        onOpen: onOpenGuestNavbarClick,
        onOpenChange: onOpenChangeGuestNavbarClick
    } = useDisclosure()

    /*已登录消息*/
    const [data, setData] = useState({
        title: '',
        message: ''
    });
    /*未登录消息*/
    const [dataGuest, setDataGuest] = useState({
        title: '',
        body: <></>,
    });

    /*界面加载完成事件*/
    useLayoutEffect(() => {
        /*登录验证*/
        async function fetchData() {
            const [authentication, username] = AuthenticationChecker();
            setLoading(false);//等待请求完毕再关闭Loading
            if (authentication) {
                setLoginEd(true)
                setUsername(username.toString());
            }
        }

        /*异步执行*/
        fetchData().then(r => {
        });
    }, [])


    /*搜索关键词*/
    const [searchKey, setSearchKey] = React.useState('')
    /*笔记搜索状态*/
    const [inSearch, setInSearch] = useState(false);

    return (
        loading ?
            <div className={'flex justify-center h-screen items-center bg-white dark:bg-default-200'}>
                <Spinner label="加载ing...😵‍💫" color="primary" labelColor="primary" size={"lg"}/>
            </div> :
            <div className={"flex flex-col h-screen"}>
                {/*主体NavBar*/}
                <Navbar
                    isBordered={true}
                    isBlurred={true}
                    isMenuOpen={isMenuOpen}
                    onMenuOpenChange={setIsMenuOpen}
                    className={`dark:bg-default-100`}
                >
                    {/*标题*/}
                    <NavbarContent className="sm:hidden pr-3" justify="start">
                        <NavbarMenuToggle aria-label={isMenuOpen ? "Close menu" : "Open menu"}
                                          className="sm:hidden dark:text-white"/>
                        <NavbarBrand>
                            <NoteIcon/>
                            <p className="font-bold text-inherit ms-0.5 dark:text-white light:text-black ">wow-note</p>
                        </NavbarBrand>
                    </NavbarContent>
                    {/*标杆*/}
                    <NavbarContent className={`hidden sm:flex gap-4`} justify="center">
                        <NavbarBrand>
                            <NoteIcon/>
                            <p className="font-bold text-inherit ms-0.5 dark:text-white light:text-black ">wow-note</p>
                        </NavbarBrand>
                        <Spacer x={0.5}/>
                        {loginEd ? <>
                            <NavbarItem>
                                <Link color="foreground" href="#" onPress={onOpenShare}>
                                    我的分享
                                </Link>
                            </NavbarItem>
                            <NavbarItem>
                                <Link color={"foreground"} href="#" onPress={() => {
                                    handleNavbarClick('我的标签');
                                }}>
                                    我的标签
                                </Link>
                            </NavbarItem>
                            <NavbarItem>
                                <Link color="foreground" href="#" onPress={() => {
                                    handleNavbarClick('我的评论');
                                }}>
                                    我的评论
                                </Link>
                            </NavbarItem>
                        </> : <><NavbarItem>
                            <Link color="foreground" href="#" onPress={() => {
                                handleNavbarClick('项目介绍');
                            }}>
                                项目介绍
                            </Link>
                        </NavbarItem>
                            <NavbarItem>
                                <Link color={"foreground"} href="#" onPress={() => {
                                    handleNavbarClick('帮助中心');
                                }}>
                                    帮助中心
                                </Link>
                            </NavbarItem>
                            <NavbarItem>
                                <Link color="foreground" href="#" onPress={() => {
                                    handleNavbarClick('关于我们');
                                }}>
                                    关于我们
                                </Link>
                            </NavbarItem>
                        </>
                        }
                    </NavbarContent>


                    {/*头像部分*/}
                    {loginEd ?
                        /*已登录头像部分*/
                        <OnAuthenticatedNavbarContent username={username} isExit={isExit} setExit={setExit}
                                                      setSearchKey={setSearchKey}
                                                      setInSearch={setInSearch}/>
                        :
                        /*无登录头像部分*/
                        <UnAuthenticatedNavbarContent onOpen={onOpen}/>
                    }
                    {/*手机模式 Menu 列表*/}
                    <NavbarMenu>
                        {menuData.map((item, index) => (
                            <NavbarMenuItem key={`${item}-${index}`}>
                                <Link
                                    className="w-full"
                                    color="foreground"
                                    href="#"
                                    size="lg"
                                    onPress={() => {
                                        handleNavbarClick(item);
                                    }}
                                >
                                    {item}
                                </Link>
                            </NavbarMenuItem>
                        ))}
                    </NavbarMenu>
                    {/*登录Dialog*/}
                    <AuthModal isOpen={isOpen} onOpenChange={onOpenChange}/>
                </Navbar>
                {/*内容区域*/}
                {
                    loginEd ?
                        /*LoginEd*/
                        <Router>
                            <Profile searchKey={searchKey} setSearchKey={setSearchKey} inSearch={inSearch}
                                     setInSearch={setInSearch}/>
                        </Router>
                        :
                        /*Don’t Login*/
                        <HomeBody onOpen={onOpen}/>
                }
                {/*退出登录Modal*/}
                <ExitLogin isExit={isExit} setExit={setExit}/>
                {/*我的分享*/}
                <ShareComponents isOpenShare={isOpenShare} onOpenChangeShare={onOpenChangeShare}/>
                {/*我的标签和我的评论组件*/}
                <UserNavbarComponents isOpenNavBarClick={isOpenNavbarClick}
                                      onOpenChangeNavBarClick={onOpenChangeNavbarClick} title={data.title}
                                      message={data.message}/>
                {/*未登录Navbar组件*/}
                <GuestNavbarComponents isOpenGuestNavbarClick={isOpenGuestNavbarClick}
                                       onOpenChangeGuestNavbarClick={onOpenChangeGuestNavbarClick}
                                       title={dataGuest.title}
                                       body={dataGuest.body}/>
            </div>
    );

    /*点击时间*/
    function handleNavbarClick(item: any) {
        switch (item) {
            case '我的分享':
                if (loginEd) {
                    onOpenShare();
                    /**
                     * 关闭Item
                     */
                    setIsMenuOpen(false);
                }
                break;

            case '我的标签':
                if (loginEd) {
                    //指定消息
                    setData({
                        title: '我创建的标签 🔖',
                        message: '标签模块等待和你一起开发 🦀'
                    });
                }
                NavbarClick();
                break;

            case '我的评论':
                if (loginEd) {
                    //指定消息
                    setData({
                        title: '我评价的评论 🍵',
                        message: '评论块等待和你一起开发 🦜'
                    });
                }
                NavbarClick();
                break;

            case '项目介绍':
                //指定消息
                setDataGuest({
                    title: '关于项目',
                    body: <ViewMD source={1}/>
                });

                GuestNavbarClick();

                /**
                 * 关闭Item
                 */
                setIsMenuOpen(false);
                break;

            case '帮助中心':
                //指定消息
                setDataGuest({
                    title: '帮助中心',
                    body: <ViewMD source={2}/>
                });

                GuestNavbarClick();
                break;

            default:
                //指定消息
                setDataGuest({
                    title: '关于我们',
                    body: <ViewMD source={3}/>
                });
                GuestNavbarClick();
                break;
        }
    }

    /*我的标签和我的评论（待开发）*/
    function NavbarClick() {
        onOpenNavbarClick();
    }

    /*未登录点击时间（待开发）*/
    function GuestNavbarClick() {
        onOpenGuestNavbarClick();
    }
}