# wow-note 个人云储笔记

<div align=center><img src="https://pic8.58cdn.com.cn/nowater/webim/big/n_v29caf8ea18a04418593d89acf3c35b8f6.png#pic_center" width="200"></div>

> 本仓库为前端仓库，需要了解后端仓库请🍧[https://gitee.com/uyevan/wow-note-spring](https://gitee.com/uyevan/wow-note-spring)
>> 您也可以在线预览成品🎉[https://note.jfkj.xyz](https://note.jfkj.xyz)

#### 项目简介 👨‍💻

>  Wow-note 个人云储笔记项目是专门用来在线 **存储** 和 **分享** 个人笔记文档的软件；使用者可以快速地进行创建文件夹、笔记等操作，也可以对其进行 **分享** 操作，以便更多人查看与协助此笔记。开发此软件的根本目的是对个人以前所学的技术进行一个综合的总结，无论是在以前在学校教过或自学过的相关技术进行一个打捞和评估；其次是本项目可能会作为我的毕设项目，虽然我写过很多项目（**UY云、UyClub工具箱、乐应软件库、LanzouAPI**...）但无论从技术层面、开发成本和实现难度上来看 WowNote 这项目都比这些项目复杂得多。   
>  wow-note项目后端是基于 SpringCloud 微服务方案扩展；前端是基于 NextJS（基于React的服务端渲染框架） 框架进行扩展。我个人开发流程是 **设计数据库 -> 前后端总体架构搭建 -> 边编码|边测试 -> 整体测试和预览 -> 部署**，虽然不能说特别牛x，但对于我个人而言从设计到现在为止收获满满，毕竟现在满脑子都是各种离谱Bug，如果对你有帮助不妨给个Star。
> ![项目简介](https://pic8.58cdn.com.cn/nowater/webim/big/n_v2f317a9891a0c4fd4a9ce576d1a9e1e83.png#pic_center)


项目架构 🪜
> ![项目架构图](https://pic6.58cdn.com.cn/nowater/webim/big/n_v2a83a00615d9a439b91acfa6c51d857b5.png#pic_center)

应用结构 🛎️
>![应用结构](https://pic8.58cdn.com.cn/nowater/webim/big/n_v2413c4d31ee95498a9120665660daeb65.png#pic_center)

#### 功能特性 🎉
> - 服务端渲染：NextJS是基于React的服务端渲染框架，在此项目开发中我使用到了一些组件在服务端渲染，特性就是快速渲染、静态站点生成也有利于CEO优化。
> - NextJS钩子：NextJS提供了很多额外的API和生命周期函数，如对 **Router** 的优化， **useEffect** 钩子等等。
> - 新概念UI：在项目的UI选择上我并未选择原生的渲染，而是选择了 **NextUI** 框架，因为它提供了主流的组件与丰富的样式供我们选择，最重要的是各种组件做的都特别好看。

#### 环境要求 ✅
**NodeJS** [https://nodejs.org](https://nodejs.org/)
> v18.17.0 (npm: '9.6.7')

**React** [https://react.docschina.org](https://react.docschina.org)
> 18.2.0

**Next.js** [https://nextjs.org](https://nextjs.org/)
> Next.js v14.0.4

**NextUI** [https://nextui.org](https://nextui.org/)
> "nextui-org/accordion": "v2.0.28",   
    "nextui-org/react": "v2.2.9"

**Docker** [https://www.docker.com](https://www.docker.com/)
> Cloud integration: v1.0.35+desktop.5   
 Version:           24.0.6   
 API version:       1.43   
 Go version:        go1.20.7   
 OS/Arch:           windows/amd64

#### 技术栈 🍵
 - TypeScript
 - React
 - Next.js
 - NextUI
 - TailWindCSS
 - Docker

#### 快速开始 🔛
>  我首先是在电脑（Windows11）本地进行开发，早期我是通过Docker打包镜像，再放到服务器进行部署的，可以通过拉取本地进行调试，也可以通过Docker进行拉去调试；
>  目前是通过 `Jenkins` CI/CD工具进行自动化部署，我只需本地开发、调试、测试之后 Push 到仓库即可，它将全程自动部署；

💻 本地拉去运行
> - 环境配置：配置本地 **NodeJS** 环境；
> - 拉取项目：`git clone https://gitee.com/uyevan/wow-note-react.git `；
> - 应用配置：防止跨域问题我加了应用代理的，你可以在 **next.config.js** 文件内修改代理；应用服务域名可以在 **app/constants/openApiConstants.tsx** 中更改；
> - 运行项目：打开终端执行 `npm i --save --force` 进行依赖安装，你可能好奇为什么不用yarn一个包管理工具，因为我以前一直用的npm！执行后需要等待，过程比较慢要安装 **react/nextJS/nextUI** 等较大的框架，然后就执行 `npm run dev` 后打开调试服务IP进行其他操作了；如果你想上线可以执行 `npm run build` 进行构建项目，再运行 `npm run start` 进行启动；

🦣 拉去镜像运行
> - 环境配置：无论你是什么系统，要确保 **Docker** 环境并且正常启动；并且你要保证有足够空间，因为整个进行较大（2.5GB上下）,因为国内无法访问DockerHub，所以我这里使用了阿里云镜像仓库服务（个人免费），无论哪个只要有Docker环境即可；
> - 拉去镜像：`docker pull registry.cn-shenzhen.aliyuncs.com/wow-note-react/wow-note-react:latest`；
> - 运行镜像：`docker run -d -p 3000:3000 --name wow-note-react registry.cn-shenzhen.aliyuncs.com/wow-note-react/wow-note-react:latest`；
> - 运行测试：如果是本地则可以访问 **localhost:3000** 进行验证；如果是服务器可以先开放某个端口再映射这个端口启动容器，再进行验证；

#### 项目结构 🗃️
🐝 我把组件，页和服务都分开创建了，分的其实也不是特别细，感觉还可以再细分更多组件，主要是有些功能实现起来很麻烦，代码量较大，不细分组件可能后期维护根本看不懂，就比如文件夹路由导航功能；主要结构如下：
> └─app   
├─assets   
│  ├─css           // 样式表文件   
│  ├─data          // 静态数据文件   
│  └─icons         // 图标文件   
├─components   
│  ├─auth          // 身份验证组件   
│  ├─home          // 主页组件   
│  └─profile       // 用户资料组件   
│     ├─folder         
│     ├─note           
│     └─share         
├─config           // 配置文件   
├─constants        // 常量文件   
├─pages   
│  ├─auth          // 身份验证页面   
│  ├─profile       // 用户页面   
│  └─share         // 分享页面   
├─service   
│  ├─components    // 服务组件   
│  └─view          // 视图相关的服务文件   
├─utils            // 工具函数和辅助函数   
└─view             // 视图文件

🛠️ 其他配置文件跟 React 应用不分上下；

#### 项目预览 💻
> NextUI是支持全段自定义适配的，以下是电脑端的预览结果，手机端也差不多、不分上下；
> ![白日主页](https://pic8.58cdn.com.cn/nowater/webim/big/n_v2bdac3d169e3f4a27b9a22c70a960a073.png#pic_center)
> ![注册中心](https://pic3.58cdn.com.cn/nowater/webim/big/n_v2df41b1c318f846f08f347a9e869ed231.png#pic_center)
> ![文档中心](https://pic4.58cdn.com.cn/nowater/webim/big/n_v21898aa6276714379b4f27cd46bdc742a.png#pic_center)
> ![笔记编辑](https://pic4.58cdn.com.cn/nowater/webim/big/n_v2132938aa8aef4b1f919a7a2b88a488cf.png#pic_center)
> ![分享中心](https://pic7.58cdn.com.cn/nowater/webim/big/n_v27ed24111f1b048ef8fb989ba3fe0712c.png#pic_center)
> ![分享查看](https://pic6.58cdn.com.cn/nowater/webim/big/n_v234fae56962d542d3ac482a841aab17e9.png#pic_center)
> ![关于我们](https://pic5.58cdn.com.cn/nowater/webim/big/n_v22675e92a47c74950b7d45c5f1d4e4022.png#pic_center)
> ![黑夜主页](https://pic5.58cdn.com.cn/nowater/webim/big/n_v29cc1fb0de7f546cba5df989ba5d2f2cf.png#pic_center)
> ![文档中心](https://pic4.58cdn.com.cn/nowater/webim/big/n_v248a2869db600482f923b36c1cec54a32.png#pic_center)
> ![编辑文档](https://pic1.58cdn.com.cn/nowater/webim/big/n_v2bf156d28b7264e4e90669923f687906f.png#pic_center)
> ![查看文档](https://pic7.58cdn.com.cn/nowater/webim/big/n_v2e4dc286781c347169317170702c47c15.png#pic_center)

#### 其他说明 🤥
> 本项目是我学习微服务的过程中写出的一个项目，基本功能功能已经实现。如果对你有帮助可以给个 **Star** 噢！如果你觉得还有可优化的地方可以提交 Pull Request 到仓库，欢迎你来参与~
>
> <img src="https://pic2.58cdn.com.cn/nowater/webim/big/n_v2c2d0b4c563cd4de2b99a6c5a4af310f3.png" width="200" alt="加我V"/>