FROM node:18.17.0-alpine AS base
WORKDIR /app
COPY . .
RUN npm install --save
EXPOSE 3000

# Prod
RUN npm run build
#ENTRYPOINT ["node_modules/.bin/next", "start"]
# 上下启动一个意思.📌
ENTRYPOINT ["npm","run","start"]

# Dev 如用CMD后面加参数会被覆盖CMD原命令而ENTRYPOINT不会，而会原有命令后追加.
#ENTRYPOINT ["npm","run","dev"]